﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Api.Models;
using Api.Tests.Helpers;
using Api.ViewModels;
using Xunit;

namespace Api.Tests.Controllers
{
    public class UserControllerTests
    {
        [Fact]
        public async Task CanGetList()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdeuser1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "fuuser1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var user = new UserCreateViewModel
            {
                Email = "ig@test1.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateUserInDatabase(consortium.Code, functionalUnit.Code, "userId1", user);

            var user2 = new UserCreateViewModel
            {
                Email = "ig@test2.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateUserInDatabase(consortium.Code, functionalUnit.Code, "userId2", user2);


            await ApiTester.Run(new Get<IEnumerable<UserViewModel>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Count() >= 2);
                    Assert.Contains(content, x => x.Email == "ig@test2.com");
                }
            });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenFetchAllIsEmpty()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdeuser2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "fuuser2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            await ApiTester.Run(new Get<UserViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanGetOne()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdeuser3",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "fuuser3",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var user = new UserCreateViewModel
            {
                Email = $"{Guid.NewGuid():N}@test3.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            var userId = string.Empty;

            await ApiTester.Run(new Post<UserCreateViewModel, UserViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => user,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) => { userId = content.UserId; }
            },
                new Get<UserViewModel>
                {
                    Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user/{userId}",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    ExpectedStatusCode = HttpStatusCode.OK,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.Equal(content.UserId, userId);
                        Assert.Equal(content.ConsortiumCode, consortium.Code);
                        Assert.Equal(content.FunctionalUnitCode, functionalUnit.Code);
                        Assert.Equal(content.Email, user.Email);
                        Assert.Equal(content.LastName, user.LastName);
                        Assert.Equal(content.FirstName, user.FirstName);
                        Assert.Equal(content.PhoneNumber, user.PhoneNumber);
                        Assert.Equal(content.CreatedOn, user.CreatedOn);
                        Assert.Equal(content.CreatedBy, user.CreatedBy);
                        Assert.Equal(content.UpdatedBy, user.CreatedBy);
                        Assert.Equal(content.UpdatedOn, user.CreatedOn);
                        Assert.Equal(content.Consortium, consortium.Name);
                        Assert.Equal(content.FunctionalUnit, functionalUnit.Designation);
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenGettingByUnknownId()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdeuser4",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "fuuser4",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            await ApiTester.Run(new Get<UserViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user/0",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanCreateUsers()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdeuser5",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "fuuser5",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var user = new UserCreateViewModel
            {
                Email = $"{Guid.NewGuid():N}@test5.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await ApiTester.Run(new Post<UserCreateViewModel, UserViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => user,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) =>
                {
                    Assert.Equal($"http://localhost/v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user/{content.UserId}", response.Headers.Location.ToString());

                    Assert.NotEmpty(content.UserId);
                    Assert.Equal(content.ConsortiumCode, consortium.Code);
                    Assert.Equal(content.FunctionalUnitCode, functionalUnit.Code);
                    Assert.Equal(content.Email, user.Email);
                    Assert.Equal(content.LastName, user.LastName);
                    Assert.Equal(content.FirstName, user.FirstName);
                    Assert.Equal(content.PhoneNumber, user.PhoneNumber);
                    Assert.Equal(content.CreatedOn, user.CreatedOn);
                    Assert.Equal(content.CreatedBy, user.CreatedBy);
                    Assert.Equal(content.UpdatedBy, user.CreatedBy);
                    Assert.Equal(content.UpdatedOn, user.CreatedOn);
                    Assert.Equal(content.Consortium, consortium.Name);
                    Assert.Equal(content.FunctionalUnit, functionalUnit.Designation);
                    Assert.NotNull(content.PictureUrl);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnBadRequestWhenValidationErrorsOnCreate()
        {
            var user = new UserCreateViewModel();

            await ApiTester.Run(new Post<UserCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => "v1.0/consortium/0/functionalunit/0/user",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => user,
                ExpectedStatusCode = HttpStatusCode.BadRequest,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Count == 8);

                    Assert.True(content.ContainsKey("FunctionalUnitCode"));
                    Assert.True(content.ContainsKey("Email"));
                    Assert.True(content.ContainsKey("PhoneNumber"));
                    Assert.True(content.ContainsKey("FirstName"));
                    Assert.True(content.ContainsKey("LastName"));
                    Assert.True(content.ContainsKey("CreatedBy"));
                    Assert.True(content.ContainsKey("CreatedOn"));
                    Assert.True(content.ContainsKey("ConsortiumCode"));
                }
            });
        }

        [Fact]
        public async Task ShouldReturnConflictWhenCodeExistsForConsortiumOnCreate()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdeuser6",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "fuuser6",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            var user = new UserCreateViewModel
            {
                Email = "ig@test6.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            var user2 = new UserCreateViewModel
            {
                Email = "ig@test6.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);
            await Extensions.CreateUserInDatabase(consortium.Code, functionalUnit.Code, Guid.NewGuid().ToString(), user);

            await ApiTester.Run(new Post<UserCreateViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}/user",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => user2,
                ExpectedStatusCode = HttpStatusCode.Conflict,
            });
        }
    }
}