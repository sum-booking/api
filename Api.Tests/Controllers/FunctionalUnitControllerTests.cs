﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Api.Models;
using Api.Tests.Helpers;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Xunit;

namespace Api.Tests.Controllers
{
    public class FunctionalUnitControllerTests
    {
        [Fact]
        public async Task CanCreateFunctionalUnit()
        {
            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "fu0"
            };

            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdefu",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Post<FunctionalUnitCreateViewModel, FunctionalUnit>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => functionalUnit,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) =>
                {
                    Assert.Equal($"http://localhost/v1.0/consortium/{consortium.Code}/functionalunit/{content.Code}", response.Headers.Location.ToString());

                    Assert.Equal(content.Designation, functionalUnit.Designation);
                    Assert.Equal(content.Code, functionalUnit.Code);
                    Assert.Equal(content.CreatedBy, functionalUnit.CreatedBy);
                    Assert.Equal(content.CreatedOn, functionalUnit.CreatedOn);
                    Assert.Equal(content.UpdatedBy, functionalUnit.CreatedBy);
                    Assert.Equal(content.UpdatedOn, functionalUnit.CreatedOn);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnBadRequestWhenValidationErrorsOnCreate()
        {
            var functionalUnit = new FunctionalUnitCreateViewModel();
            var badCodeFunctionalUnit = new FunctionalUnitCreateViewModel { Code = "123456789" };

            await ApiTester.Run(new Post<FunctionalUnitCreateViewModel, IDictionary<string, string>>
                {
                    Uri = () => $"v1.0/consortium/0/functionalunit",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    Content = () => functionalUnit,
                    ExpectedStatusCode = HttpStatusCode.BadRequest,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.True(content.Keys.Count == 5);

                        Assert.True(content.ContainsKey("Code"));
                        Assert.True(content.ContainsKey("Designation"));
                        Assert.True(content.ContainsKey("CreatedBy"));
                        Assert.True(content.ContainsKey("CreatedOn"));
                        Assert.True(content.ContainsKey("ConsortiumCode"));
                    }
                },
                new Post<FunctionalUnitCreateViewModel, IDictionary<string, string>>
                {
                    Uri = () => $"v1.0/consortium/0/functionalunit",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    Content = () => badCodeFunctionalUnit,
                    ExpectedStatusCode = HttpStatusCode.BadRequest,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.True(content.Keys.Count == 5);

                        Assert.True(content.ContainsKey("Code"));
                        Assert.True(content.ContainsKey("Designation"));
                        Assert.True(content.ContainsKey("CreatedBy"));
                        Assert.True(content.ContainsKey("CreatedOn"));
                        Assert.True(content.ContainsKey("ConsortiumCode"));
                        Assert.True(content["Code"] == "Must be less than 8 chars");
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnConflictWhenCodeExistsForConsortiumOnCreate()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdefu1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "fu1"
            };

            await Extensions.CreateConsortiumInDatabase(consortium);
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            await ApiTester.Run(new Post<FunctionalUnitCreateViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => functionalUnit,
                ExpectedStatusCode = HttpStatusCode.Conflict,
            });
        }

        [Fact]
        public async Task CanGetOne()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdefu2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "fu2"
            };

            await ApiTester.Run(new Post<FunctionalUnitCreateViewModel>
                {
                    Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    Content = () => functionalUnit,
                    ExpectedStatusCode = HttpStatusCode.Created,
                },
                new Get<FunctionalUnit>
                {
                    Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/{functionalUnit.Code}",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    ExpectedStatusCode = HttpStatusCode.OK,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.Equal(content.Designation, functionalUnit.Designation);
                        Assert.Equal(content.Code, functionalUnit.Code);
                        Assert.Equal(content.ConsortiumCode, consortium.Code);
                        Assert.Equal(content.CreatedOn, functionalUnit.CreatedOn);
                        Assert.Equal(content.CreatedBy, functionalUnit.CreatedBy);
                        Assert.Equal(content.UpdatedBy, functionalUnit.CreatedBy);
                        Assert.Equal(content.UpdatedOn, functionalUnit.CreatedOn);
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenGettingByUnknownId()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdefu3",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Get<Consortium>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit/0",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanGetList()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdefu4",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "cdefu4",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            var functionalUnit2 = new FunctionalUnitCreateViewModel
            {
                Designation = "test",
                Code = "cdefu5",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit2);

            await ApiTester.Run(new Get<IEnumerable<FunctionalUnit>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Count() >= 2);
                    Assert.Contains(content, x => x.Designation == "test");
                }
            });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenFetchAllIsEmpty()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cdefu5",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Get<Consortium>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/functionalunit",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

    }
}