﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Api.Models;
using Api.Tests.Helpers;
using Api.ViewModels;
using Xunit;

namespace Api.Tests.Controllers
{
    public class AdminControllerTests
    {
        [Fact]
        public async Task CanGetList()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var consortium2 = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium2);

            var consortium3 = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc3",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium3);

            var user = new AdministratorCreateViewModel
            {
                Email = "ig@test1.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateAdministratorInDatabase("admusr1", user);

            var user2 = new AdministratorCreateViewModel
            {
                Email = "ig@test2.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateAdministratorInDatabase("admusr2", user2);

            await Extensions.LinkAdministratorToConsortium("admc1", "admusr1");
            await Extensions.LinkAdministratorToConsortium("admc2", "admusr1");
            await Extensions.LinkAdministratorToConsortium("admc3", "admusr2");

            await ApiTester.Run(new Get<IEnumerable<AdministratorViewModel>>
            {
                Uri = () => "v1.0/admin",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Count() >= 2);
                    Assert.Contains(content, x => x.Email == "ig@test2.com");
                }
            });
        }

        [Fact]
        public async Task CanGetOne()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc4",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var consortium2 = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc5",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium2);

            var administrator = new AdministratorCreateViewModel
            {
                Email = $"{Guid.NewGuid():N}@test3.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                ConsortiumCodes = new List<string> { "admc4", "admc5" }
            };
            var administratorId = string.Empty;

            await ApiTester.Run(new Post<AdministratorCreateViewModel, AdministratorViewModel>
            {
                Uri = () => "v1.0/admin",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => administrator,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) => { administratorId = content.AdministratorId; }
            },
                new Get<AdministratorViewModel>
                {
                    Uri = () => $"v1.0/admin/{administratorId}",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    ExpectedStatusCode = HttpStatusCode.OK,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.Equal(content.AdministratorId, administratorId);
                        Assert.Equal(content.Email, administrator.Email);
                        Assert.Equal(content.LastName, administrator.LastName);
                        Assert.Equal(content.FirstName, administrator.FirstName);
                        Assert.Equal(content.PhoneNumber, administrator.PhoneNumber);
                        Assert.Equal(content.CreatedOn, administrator.CreatedOn);
                        Assert.Equal(content.CreatedBy, administrator.CreatedBy);
                        Assert.Equal(content.UpdatedBy, administrator.CreatedBy);
                        Assert.Equal(content.UpdatedOn, administrator.CreatedOn);
                        Assert.Contains(consortium.Name, content.Consortiums.Select(x => x.Name));
                        Assert.Contains(consortium2.Name, content.Consortiums.Select(x => x.Name));
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenGettingByUnknownId()
        {

            await ApiTester.Run(new Get<AdministratorViewModel>
            {
                Uri = () => "v1.0/admin/0",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanCreateAdmin()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc6",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var consortium2 = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc7",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium2);

            var admin = new AdministratorCreateViewModel
            {
                Email = $"{Guid.NewGuid():N}@test5.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                ConsortiumCodes = new List<string> { "admc6", "admc7" }
            };

            await ApiTester.Run(new Post<AdministratorCreateViewModel, AdministratorViewModel>
            {
                Uri = () => "v1.0/admin",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => admin,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) =>
                {
                    Assert.Equal($"http://localhost/v1.0/admin/{content.AdministratorId}", response.Headers.Location.ToString());

                    Assert.NotEmpty(content.AdministratorId);
                    Assert.Equal(content.Email, admin.Email);
                    Assert.Equal(content.LastName, admin.LastName);
                    Assert.Equal(content.FirstName, admin.FirstName);
                    Assert.Equal(content.PhoneNumber, admin.PhoneNumber);
                    Assert.Equal(content.CreatedOn, admin.CreatedOn);
                    Assert.Equal(content.CreatedBy, admin.CreatedBy);
                    Assert.Equal(content.UpdatedBy, admin.CreatedBy);
                    Assert.Equal(content.UpdatedOn, admin.CreatedOn);
                    Assert.NotNull(content.PictureUrl);
                    Assert.Contains(consortium.Name, content.Consortiums.Select(x => x.Name));
                    Assert.Contains(consortium2.Name, content.Consortiums.Select(x => x.Name));
                }
            });
        }

        [Fact]
        public async Task ShouldReturnBadRequestWhenValidationErrorsOnCreate()
        {
            var admin = new AdministratorCreateViewModel();

            await ApiTester.Run(new Post<AdministratorCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => "v1.0/admin",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => admin,
                ExpectedStatusCode = HttpStatusCode.BadRequest,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Count == 7);

                    Assert.True(content.ContainsKey("Email"));
                    Assert.True(content.ContainsKey("PhoneNumber"));
                    Assert.True(content.ContainsKey("FirstName"));
                    Assert.True(content.ContainsKey("LastName"));
                    Assert.True(content.ContainsKey("CreatedBy"));
                    Assert.True(content.ContainsKey("CreatedOn"));
                    Assert.True(content.ContainsKey("ConsortiumCodes"));
                }
            });
        }

        [Fact]
        public async Task ShouldReturnConflictWhenEmailExistsOnCreate()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "admc8",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            var admin = new AdministratorCreateViewModel
            {
                Email = "ig@test6.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate(),
            };

            var admin2 = new AdministratorCreateViewModel
            {
                Email = "ig@test6.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                ConsortiumCodes = new List<string> { "admc8" }
            };

            await Extensions.CreateConsortiumInDatabase(consortium);
            await Extensions.CreateAdministratorInDatabase("conflictAdm", admin);

            await ApiTester.Run(new Post<AdministratorCreateViewModel>
            {
                Uri = () => "v1.0/admin",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => admin2,
                ExpectedStatusCode = HttpStatusCode.Conflict,
            });
        }
    }
}