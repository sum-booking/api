﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Api.Models;
using Api.Models.Enums;
using Api.Tests.Helpers;
using Api.ViewModels;
using Xunit;

namespace Api.Tests.Controllers
{
    public class FacilityControllerTests
    {
        [Fact]
        public async Task CanCreateFacility()
        {
            var facility = new FacilityCreateViewModel()
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Name = "test fu",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                BookingSchedule = BookingScheduleStrategy.MidDayAndNight
            };

            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "consfa",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Post<FacilityCreateViewModel, Facility>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/facility",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => facility,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) =>
                {
                    Assert.Equal($"http://localhost/v1.0/consortium/{consortium.Code}/facility/{content.Id}", response.Headers.Location.ToString());

                    Assert.NotEqual(0, content.Id);
                    Assert.Equal(content.Name, facility.Name);
                    Assert.Equal(content.BookingLimit, facility.BookingLimit);
                    Assert.Equal(content.BookingSchedule, facility.BookingSchedule);
                    Assert.Equal(content.CreatedBy, facility.CreatedBy);
                    Assert.Equal(content.CreatedOn, facility.CreatedOn);
                    Assert.Equal(content.UpdatedBy, facility.CreatedBy);
                    Assert.Equal(content.UpdatedOn, facility.CreatedOn);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnBadRequestWhenValidationErrorsOnCreate()
        {
            var facility = new FacilityCreateViewModel();

            await ApiTester.Run(new Post<FacilityCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => "v1.0/consortium/0/facility",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => facility,
                ExpectedStatusCode = HttpStatusCode.BadRequest,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Count == 4);

                    Assert.True(content.ContainsKey("Name"));
                    Assert.True(content.ContainsKey("CreatedBy"));
                    Assert.True(content.ContainsKey("CreatedOn"));
                    Assert.True(content.ContainsKey("ConsortiumCode"));
                }
            });
        }

        [Fact]
        public async Task ShouldReturnConflictWhenNameExistsForConsortiumOnCreate()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "consfa1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            var facility = new FacilityCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Name = "test conflict",
            };

            await Extensions.CreateConsortiumInDatabase(consortium);
            await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            await ApiTester.Run(new Post<FacilityCreateViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/facility",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => facility,
                ExpectedStatusCode = HttpStatusCode.Conflict,
            });
        }

        [Fact]
        public async Task CanGetOne()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "consfa2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            var facility = new FacilityCreateViewModel()
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Name = "test get",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                BookingSchedule = BookingScheduleStrategy.NightOnly,
            };
            var facilityId = 0;

            await ApiTester.Run(new Post<FacilityCreateViewModel, Facility>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/facility",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => facility,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) => { facilityId = content.Id; }
            },
                new Get<Facility>
                {
                    Uri = () => $"v1.0/consortium/{consortium.Code}/facility/{facilityId}",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    ExpectedStatusCode = HttpStatusCode.OK,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.Equal(content.Name, facility.Name);
                        Assert.Equal(content.Id, facilityId);
                        Assert.Equal(content.BookingLimit, facility.BookingLimit);
                        Assert.Equal(content.BookingSchedule, facility.BookingSchedule);
                        Assert.Equal(content.ConsortiumCode, consortium.Code);
                        Assert.Equal(content.CreatedOn, facility.CreatedOn);
                        Assert.Equal(content.CreatedBy, facility.CreatedBy);
                        Assert.Equal(content.UpdatedBy, facility.CreatedBy);
                        Assert.Equal(content.UpdatedOn, facility.CreatedOn);
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenGettingByUnknownId()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "consfa3",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Get<Facility>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/facility/0",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanGetList()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "consfa4",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            var facility = new FacilityCreateViewModel()
            {
                Name = "test list",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            var facility2 = new FacilityCreateViewModel
            {
                Name = "test list 2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateFacilityInDatabase(consortium.Code, facility);
            await Extensions.CreateFacilityInDatabase(consortium.Code, facility2);

            await ApiTester.Run(new Get<IEnumerable<Facility>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/facility",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Count() >= 2);
                    Assert.Contains(content, x => x.Name == "test list");
                }
            });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenFetchAllIsEmpty()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "consfa5",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Get<Consortium>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/facility",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

    }
}