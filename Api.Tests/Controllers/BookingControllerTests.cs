﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Api.Models.Enums;
using Api.Tests.Helpers;
using Api.ViewModels;
using Xunit;

namespace Api.Tests.Controllers
{
    public class BookingControllerTests
    {
        [Fact]
        public async Task CanCreateBooking()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco0",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofu0"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility booking 0",
                BookingSchedule = BookingScheduleStrategy.NightOnly
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddDays(8).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };

            await ApiTester.Run(new Post<BookingCreateViewModel, BookingViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) =>
                {
                    Assert.Equal($"http://localhost/v1.0/consortium/{consortium.Code}/booking/{content.Id}", response.Headers.Location.ToString());

                    Assert.Equal(content.FacilityName, facility.Name);
                    Assert.Equal(content.FunctionalUnitDesignation, functionalUnit.Designation);
                    Assert.Equal(content.BookDate, booking.BookDate);
                    Assert.Equal(content.Strategy, facility.BookingSchedule);
                    Assert.Equal(content.ConsortiumCode, consortium.Code);
                    Assert.False(content.Deleted);
                    Assert.Equal(content.FacilityId, facilityId);
                    Assert.Equal(content.FunctionalUnitCode, functionalUnit.Code);
                    Assert.Null(content.IsMidDay);
                    Assert.Null(content.IsNight);
                    Assert.Equal(content.CreatedBy, functionalUnit.CreatedBy);
                    Assert.Equal(content.CreatedOn, functionalUnit.CreatedOn);
                    Assert.Equal(content.UpdatedBy, functionalUnit.CreatedBy);
                    Assert.Equal(content.UpdatedOn, functionalUnit.CreatedOn);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnBadRequestWhenValidationErrorsOnCreate()
        {
            var booking = new BookingCreateViewModel();

            await ApiTester.Run(new Post<BookingCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => "v1.0/consortium/0/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking,
                ExpectedStatusCode = HttpStatusCode.BadRequest,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Count == 6);

                    Assert.True(content.ContainsKey("BookDate"));
                    Assert.True(content.ContainsKey("CreatedBy"));
                    Assert.True(content.ContainsKey("CreatedOn"));
                    Assert.True(content.ContainsKey("ConsortiumCode"));
                    Assert.True(content.ContainsKey("FunctionalUnitCode"));
                    Assert.True(content.ContainsKey("FacilityId"));

                }
            });
        }

        [Fact]
        public async Task ShouldReturnConflictWhenTwoMonthsLimitReachOnCreate()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofu1"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility booking 1",
                BookingSchedule = BookingScheduleStrategy.NightOnly
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            var booking2 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddDays(1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking2);

            var booking3 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddDays(-1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };

            await ApiTester.Run(new Post<BookingCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking3,
                ExpectedStatusCode = HttpStatusCode.Conflict,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Contains("TWICE_A_MONTH"));
                    Assert.Equal(1, content.Keys.Count);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnConflictIfBookingSameNight()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco6",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofu6"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var user2 = new UserCreateViewModel
            {
                Email = "ig@boofu6.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateUserInDatabase(consortium.Code, functionalUnit.Code, "boofu6user", user2);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility booking 6",
                BookingSchedule = BookingScheduleStrategy.NightOnly
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            var booking2 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };

            await ApiTester.Run(new Post<BookingCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking2,
                ExpectedStatusCode = HttpStatusCode.Conflict,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Contains("ALREADY_BOOKED"));
                    Assert.Equal(1, content.Keys.Count);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnConflictIfAlreadyBookedSameMidDayAndNight()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco7",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofu7"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var user = new UserCreateViewModel
            {
                Email = "ig@boofu7.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateUserInDatabase(consortium.Code, functionalUnit.Code, "boofu7user", user);

            var functionalUnit2 = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofu8"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit2);

            var user2 = new UserCreateViewModel
            {
                Email = "ig@boofu8.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateUserInDatabase(consortium.Code, functionalUnit2.Code, "boofu8user", user2);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility booking 7",
                BookingSchedule = BookingScheduleStrategy.MidDayAndNight
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
                IsMidDay = true,
                IsNight = false,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            var booking2 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
                IsNight = true,
                IsMidDay = false,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking2);

            var booking3 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit2.Code,
                IsNight = true,
                IsMidDay = false,
            };

            await ApiTester.Run(new Post<BookingCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking3,
                ExpectedStatusCode = HttpStatusCode.Conflict,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Contains("ALREADY_BOOKED"));
                    Assert.Equal(1, content.Keys.Count);
                }
            });
        }

        [Fact]
        public async Task CanGetOne()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofu2"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility booking 2",
                BookingSchedule = BookingScheduleStrategy.NightOnly
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddDays(8).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            var bookingId = 0;

            await ApiTester.Run(new Post<BookingCreateViewModel, BookingViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) => { bookingId = content.Id; }
            },
                new Get<BookingViewModel>
                {
                    Uri = () => $"v1.0/consortium/{consortium.Code}/booking/{bookingId}",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    ExpectedStatusCode = HttpStatusCode.OK,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.Equal(content.Id, bookingId);
                        Assert.Equal(content.FacilityName, facility.Name);
                        Assert.Equal(content.FunctionalUnitDesignation, functionalUnit.Designation);
                        Assert.Equal(content.BookDate, booking.BookDate);
                        Assert.Equal(content.Strategy, facility.BookingSchedule);
                        Assert.Equal(content.ConsortiumCode, consortium.Code);
                        Assert.False(content.Deleted);
                        Assert.Equal(content.FacilityId, facilityId);
                        Assert.Equal(content.FunctionalUnitCode, functionalUnit.Code);
                        Assert.Null(content.IsMidDay);
                        Assert.Null(content.IsNight);
                        Assert.Equal(content.CreatedBy, functionalUnit.CreatedBy);
                        Assert.Equal(content.CreatedOn, functionalUnit.CreatedOn);
                        Assert.Equal(content.UpdatedBy, functionalUnit.CreatedBy);
                        Assert.Equal(content.UpdatedOn, functionalUnit.CreatedOn);
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenGettingByUnknownId()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco3",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Get<BookingViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking/0",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanGetList()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco4",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofu4"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var user = new UserCreateViewModel
            {
                Email = "ig@boofu4.com",
                LastName = "test family",
                FirstName = "given",
                PhoneNumber = "+57865564",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateUserInDatabase(consortium.Code, functionalUnit.Code, "boofu4user", user);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility booking 4",
                BookingSchedule = BookingScheduleStrategy.NightOnly
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            var booking2 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddDays(1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking2);

            await ApiTester.Run(new Get<IEnumerable<BookingViewModel>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking?from={HttpUtility.UrlEncode(DateTimeOffset.Now.AddDays(-1).ToString())}" +
                            $"&to={HttpUtility.UrlEncode(DateTimeOffset.Now.AddDays(1).ToString())}",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Count() == 2);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenFetchAllIsEmpty()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "booco5",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Get<BookingViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanCancelBooking()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "boodel",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boodel"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility delete",
                BookingSchedule = BookingScheduleStrategy.NightOnly,
                CancellationPolicy = BookingCancellationPolicy.FortyEightHours,
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddMonths(1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            var bookingId = 0;

            await ApiTester.Run(new Post<BookingCreateViewModel, BookingViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) => { bookingId = content.Id; }
            },
            new Delete
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking/{bookingId}?updatedBy=nacho&updatedOn={HttpUtility.UrlEncode(DateTimeOffset.Now.Trucate().ToString())}",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
            },
            new Get
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking/{bookingId}",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound,
            });
        }

        [Fact]
        public async Task ShouldReturnBadRequestCancel48Hours()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "boodel1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boodel1"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility delete1",
                BookingSchedule = BookingScheduleStrategy.NightOnly,
                CancellationPolicy = BookingCancellationPolicy.FortyEightHours,
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddDays(1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            var bookingId = await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            await ApiTester.Run(
                new Delete<IDictionary<string, string>>
                {
                    Uri = () => $"v1.0/consortium/{consortium.Code}/booking/{bookingId}?updatedBy=nacho&updatedOn={HttpUtility.UrlEncode(DateTimeOffset.Now.Trucate().ToString())}",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    ExpectedStatusCode = HttpStatusCode.Conflict,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.True(content.Keys.Contains("48HOURS_CANCEL"));
                        Assert.Equal(1, content.Keys.Count);
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnBadRequestCancel72Hours()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "boodel2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boodel2"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility delete2",
                BookingSchedule = BookingScheduleStrategy.NightOnly,
                CancellationPolicy = BookingCancellationPolicy.SeventyTwoHours,
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddDays(2).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            var bookingId = await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            await ApiTester.Run(
                new Delete<IDictionary<string, string>>
                {
                    Uri = () => $"v1.0/consortium/{consortium.Code}/booking/{bookingId}?updatedBy=nacho&updatedOn={HttpUtility.UrlEncode(DateTimeOffset.Now.Trucate().ToString())}",
                    AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                    ExpectedStatusCode = HttpStatusCode.Conflict,
                    AssertResponseContent = (response, content) =>
                    {
                        Assert.True(content.Keys.Contains("72HOURS_CANCEL"));
                        Assert.Equal(1, content.Keys.Count);
                    }
                });
        }

        [Fact]
        public async Task ShouldReturnConflictWhenTestingAnticipationPolicy()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "boocoan",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "boofuan"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility booking anticipation",
                BookingSchedule = BookingScheduleStrategy.NightOnly
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };

            await ApiTester.Run(new Post<BookingCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking,
                ExpectedStatusCode = HttpStatusCode.Conflict,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Contains("7DAYS_ANTICIPATION"));
                    Assert.Equal(1, content.Keys.Count);
                }
            });
        }

        [Fact]
        public async Task ShouldAllowToCreateAfterRemovingOneWhenReachingLimit()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "codelfix",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Designation = "test fu",
                Code = "fudelfix"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var facility = new FacilityCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility delete fix 1",
                BookingSchedule = BookingScheduleStrategy.NightOnly
            };
            var facilityId = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var booking = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddMonths(1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            var booking2 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddMonths(1).AddDays(1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };
            var booking2Id = await Extensions.CreateBookingInDatabase(consortium.Code, booking2);

            var booking3 = new BookingCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "creator",
                FacilityId = facilityId,
                BookDate = DateTimeOffset.Now.AddMonths(1).AddDays(-1).Trucate(),
                FunctionalUnitCode = functionalUnit.Code,
            };

            await ApiTester.Run(new Post<BookingCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking3,
                ExpectedStatusCode = HttpStatusCode.Conflict,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Contains("TWICE_A_MONTH"));
                    Assert.Equal(1, content.Keys.Count);
                }
            },
            new Delete
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking/{booking2Id}?updatedBy=nacho&updatedOn={HttpUtility.UrlEncode(DateTimeOffset.Now.Trucate().ToString())}",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
            },new Post<BookingCreateViewModel>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}/booking",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => booking3,
                ExpectedStatusCode = HttpStatusCode.Created,
            });
        }
    }
}