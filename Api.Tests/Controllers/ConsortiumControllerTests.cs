using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Api.Models;
using Api.Tests.Helpers;
using Api.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Xunit;

namespace Api.Tests.Controllers
{
    public class ConsortiumControllerTests
    {
        [Fact]
        public async Task CanCreateConsortium()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Name = "test consortium",
                Code = "testCd"
            };

            await ApiTester.Run(new Post<ConsortiumCreateViewModel, Consortium>
            {
                Uri = () => "v1.0/consortium",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => consortium,
                ExpectedStatusCode = HttpStatusCode.Created,
                AssertResponseContent = (response, content) =>
                {
                    Assert.Equal($"http://localhost/v1.0/consortium/{content.Code}", response.Headers.Location.ToString());

                    Assert.Equal(content.Name, consortium.Name);
                    Assert.Equal(content.CreatedBy, consortium.CreatedBy);
                    Assert.Equal(content.CreatedOn, consortium.CreatedOn);
                    Assert.Equal(content.UpdatedBy, consortium.CreatedBy);
                    Assert.Equal(content.UpdatedOn, consortium.CreatedOn);
                }
            });
        }

        [Fact]
        public async Task ShouldGetBadRequestWhenValidationErrorsOnCreate()
        {
            var consortium = new ConsortiumCreateViewModel();
            var badCodeConsortium = new ConsortiumCreateViewModel { Code = "123456789" };

            await ApiTester.Run(new Post<ConsortiumCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => "v1.0/consortium",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => consortium,
                ExpectedStatusCode = HttpStatusCode.BadRequest,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Count == 4);

                    Assert.True(content.ContainsKey("Code"));
                    Assert.True(content.ContainsKey("Name"));
                    Assert.True(content.ContainsKey("CreatedBy"));
                    Assert.True(content.ContainsKey("CreatedOn"));
                }
            },
            new Post<ConsortiumCreateViewModel, IDictionary<string, string>>
            {
                Uri = () => "v1.0/consortium",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => badCodeConsortium,
                ExpectedStatusCode = HttpStatusCode.BadRequest,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Keys.Count == 4);

                    Assert.True(content.ContainsKey("Code"));
                    Assert.True(content.ContainsKey("Name"));
                    Assert.True(content.ContainsKey("CreatedBy"));
                    Assert.True(content.ContainsKey("CreatedOn"));
                    Assert.True(content["Code"] == "Must be less than 8 chars");
                }
            });
        }

        [Fact]
        public async Task ShouldGetConflictIfCodeAlreadyExistsOnCreate()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cde",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run(new Post<ConsortiumCreateViewModel>
            {
                Uri = () => "v1.0/consortium",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => consortium,
                ExpectedStatusCode = HttpStatusCode.Conflict,
            });
        }

        [Fact]
        public async Task CanGetOne()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                CreatedBy = "creator",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Name = "test consortium",
                Code = "testCd2"
            };

            await ApiTester.Run(new Post<ConsortiumCreateViewModel>
            {
                Uri = () => "v1.0/consortium",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                Content = () => consortium,
                ExpectedStatusCode = HttpStatusCode.Created,
            },
            new Get<Consortium>
            {
                Uri = () => $"v1.0/consortium/{consortium.Code}",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
                AssertResponseContent = (response, content) =>
                {
                    Assert.Equal(content.Name, consortium.Name);
                    Assert.Equal(content.Code, consortium.Code);
                    Assert.Equal(content.CreatedOn, consortium.CreatedOn);
                    Assert.Equal(content.CreatedBy, consortium.CreatedBy);
                    Assert.Equal(content.UpdatedBy, consortium.CreatedBy);
                    Assert.Equal(content.UpdatedOn, consortium.CreatedOn);
                }
            });
        }

        [Fact]
        public async Task ShouldReturnNotFoundWhenGettingByUnknownId()
        {
            await ApiTester.Run(new Get<Consortium>
            {
                Uri = () => "v1.0/consortium/0",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.NotFound
            });
        }

        [Fact]
        public async Task CanGetList()
        {
            var consortium = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cde1",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            var consortium2 = new ConsortiumCreateViewModel
            {
                Name = "test",
                Code = "cde2",
                CreatedBy = "test user",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };

            await Extensions.CreateConsortiumInDatabase(consortium);
            await Extensions.CreateConsortiumInDatabase(consortium2);

            await ApiTester.Run(new Get<IEnumerable<Consortium>>
            {
                Uri = () => "v1.0/consortium",
                AuthenticationHeaderValue = () => ApiTester.AuthorizationToken.ToAuthHeaderValue(),
                ExpectedStatusCode = HttpStatusCode.OK,
                AssertResponseContent = (response, content) =>
                {
                    Assert.True(content.Count() >= 2);
                    Assert.Contains(content, x => x.Name == "test");
                }
            });
        }
    }
}
