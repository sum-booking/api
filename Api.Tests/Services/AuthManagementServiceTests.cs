﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;
using Api.Services;
using Api.Tests.Helpers;
using Api.ViewModels;
using Xunit;

namespace Api.Tests.Services
{
    public class AuthManagementServiceTests
    {
        [Fact]
        public async Task CanCreateUser()
        {
            var userModel = new UserCreateViewModel
            {
                PhoneNumber = "+59897",
                LastName = "test",
                Email = $"{Guid.NewGuid():N}@test.com",
                FirstName = "user"
            };
            var consortiumCode = Guid.NewGuid().ToString("N");
            var functionalUnitCode = Guid.NewGuid().ToString("N");

            await ApiTester.Run();
            var service = new AuthManagementService(ApiTester.Configuration);

            var userId = await service.CreateUser(userModel, consortiumCode, functionalUnitCode);

            Assert.NotNull(userId);
        }

        [Fact]
        public async Task CanCreateAmin()
        {
            var model = new AdministratorCreateViewModel
            {
                PhoneNumber = "+59897",
                LastName = "test",
                Email = $"{Guid.NewGuid():N}@test.com",
                FirstName = "user",
                ConsortiumCodes = new List<string> { "test1", "test2" }
            };

            await ApiTester.Run();
            var service = new AuthManagementService(ApiTester.Configuration);

            var adminId = await service.CreateAdministrator(model);

            Assert.NotNull(adminId);
        }
    }
}