﻿using System;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Services.Limits;
using Api.Tests.Helpers;
using Api.ViewModels;
using Xunit;

namespace Api.Tests.Services.Limits
{
    public class TwiceAMonthBookingStrategyTests
    {
        [Fact]
        public async Task LimitNotTested()
        {
            await ApiTester.Run();

            var consortium = new ConsortiumCreateViewModel
            {
                Name = "consortium",
                CreatedBy = "test",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Code = "limitCo"
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var facility = new FacilityCreateViewModel
            {
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility1",
                BookingSchedule = BookingScheduleStrategy.MidDayAndNight,
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "test"
            };
            var id = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "test",
                Code = "limitfu",
                Designation = "Limit functional unit"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);

            var functionalUnit2 = new FunctionalUnitCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "test",
                Code = "limitfu2",
                Designation = "Limit functional unit"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit2);

            var booking = new BookingCreateViewModel
            {
                FunctionalUnitCode = functionalUnit.Code,
                BookDate = DateTimeOffset.Now.Trucate(),
                FacilityId = id,
                CreatedBy = "test",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            var booking2 = new BookingCreateViewModel
            {
                FunctionalUnitCode = functionalUnit2.Code,
                BookDate = DateTimeOffset.Now.Trucate(),
                FacilityId = id,
                CreatedBy = "test",
                CreatedOn = DateTimeOffset.Now.Trucate(),
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking2);

            var limitStrategy = new TwiceAMonthBookingStrategy(ApiTester.Configuration);

            var limit = await limitStrategy.TestLimitAsync(consortium.Code, booking);
            Assert.Null(limit);

            var limit2 = await limitStrategy.TestLimitAsync(consortium.Code, booking2);
            Assert.Null(limit2);

        }

        [Fact]
        public async Task LimitTested()
        {
            await ApiTester.Run();

            var consortium = new ConsortiumCreateViewModel
            {
                Name = "consortium",
                CreatedBy = "test",
                CreatedOn = DateTimeOffset.Now.Trucate(),
                Code = "limitCo2"
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            var facility = new FacilityCreateViewModel
            {
                BookingLimit = BookingLimitStrategy.TwiceAMonth,
                Name = "facility2",
                BookingSchedule = BookingScheduleStrategy.MidDayAndNight,
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "test"
            };
            var id = await Extensions.CreateFacilityInDatabase(consortium.Code, facility);

            var functionalUnit = new FunctionalUnitCreateViewModel
            {
                CreatedOn = DateTimeOffset.Now.Trucate(),
                CreatedBy = "test",
                Code = "limitfu2",
                Designation = "Limit functional unit"
            };
            await Extensions.CreateFunctionalUnitInDatabase(consortium.Code, functionalUnit);            

            var booking = new BookingCreateViewModel
            {
                FunctionalUnitCode = functionalUnit.Code,
                BookDate = DateTimeOffset.Now.Trucate(),
                FacilityId = id,
                CreatedBy = "test",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking);

            var booking2 = new BookingCreateViewModel
            {
                FunctionalUnitCode = functionalUnit.Code,
                BookDate = DateTimeOffset.Now.Trucate(),
                FacilityId = id,
                CreatedBy = "test",
                CreatedOn = DateTimeOffset.Now.Trucate(),
            };
            await Extensions.CreateBookingInDatabase(consortium.Code, booking2);

            var limitStrategy = new TwiceAMonthBookingStrategy(ApiTester.Configuration);

            var limit = await limitStrategy.TestLimitAsync(consortium.Code, booking);
            Assert.True(limit.Keys.Contains("TWICE_A_MONTH"));
            Assert.Equal(1, limit.Keys.Count);

            var limit2 = await limitStrategy.TestLimitAsync(consortium.Code, booking2);
            Assert.True(limit2.Keys.Contains("TWICE_A_MONTH"));
            Assert.Equal(1, limit2.Keys.Count);
        }
    }
}