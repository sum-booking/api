﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Api.Repositories;
using Api.Services;
using Api.Tests.Helpers;
using Api.ViewModels;
using Xunit;

namespace Api.Tests.Services
{
    public class MailServiceTest
    {
        [Fact]
        public async Task CanSendAuthenticationLink()
        {
            var userModel = new UserViewModel
            {
                PhoneNumber = "+59897",
                LastName = "test",
                Email = $"{Guid.NewGuid():N}@sumbooking.com",
                FirstName = "user"
            };

            var consortium = new ConsortiumCreateViewModel
            {
                Code = "hohohoh",
                Name = "test consortium",
                CreatedBy = "nacho",
                CreatedOn = DateTimeOffset.Now.Trucate()
            };
            await Extensions.CreateConsortiumInDatabase(consortium);

            await ApiTester.Run();
            var repository = new ConsortiumRepository(ApiTester.Configuration);
            var service = new MailService(ApiTester.Configuration, repository);

            await service.SendWelcomeEmailAsync(userModel, consortium.Code);
        }
    }
}
