﻿using System;
using System.Threading.Tasks;
using Api.Services;
using Api.ViewModels;
using Auth0.ManagementApi.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Moq;

namespace Api.Tests.Helpers
{
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration, IHostingEnvironment environment) : base(configuration, environment)
        {
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            var mockedManagementService = GetMockedManagementService();
            var mockedMailService = GetMockedMailService();
            services.Replace(ServiceDescriptor.Scoped(config => mockedManagementService));
            services.Replace(ServiceDescriptor.Scoped(config => mockedMailService));
        }

        private IMailService GetMockedMailService()
        {
            var mailServiceMock = new Mock<IMailService>();

            mailServiceMock.Setup(setup =>
                setup.SendWelcomeEmailAsync(It.IsAny<UserViewModel>(), It.IsAny<string>()))
                .Returns(Task.CompletedTask);

            return mailServiceMock.Object;
        }

        private IAuthManagementService GetMockedManagementService()
        {
            var authManagementServiceMock = new Mock<IAuthManagementService>();

            authManagementServiceMock.Setup(setup =>
                setup.CreateAdministrator(It.IsAny<AdministratorCreateViewModel>())
            ).Returns(() =>
            {
                var userId = Guid.NewGuid();
                var user = new User
                {
                    UserId = userId.ToString("N"),
                    Picture = "http://mypic.jpg"
                };
                return Task.FromResult(user);
            });

            authManagementServiceMock.Setup(setup =>
                setup.CreateUser(It.IsAny<UserCreateViewModel>(), It.IsAny<string>(), It.IsAny<string>())
            ).Returns(() =>
            {
                var userId = Guid.NewGuid();
                var user = new User
                {
                    UserId = userId.ToString("N"),
                    Picture = "http://mypic.jpg"
                };
                return Task.FromResult(user);
            });

            return authManagementServiceMock.Object;
        }
    }
}