﻿
using System;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Repositories;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Npgsql;

namespace Api.Tests.Helpers
{
    public static class Extensions
    {
        public static DateTimeOffset Trucate(this DateTimeOffset dateTimeOffset)
        {
            return dateTimeOffset.AddTicks(-(dateTimeOffset.Ticks % TimeSpan.TicksPerSecond));
        }

        public static async Task CreateConsortiumInDatabase(ConsortiumCreateViewModel consortium)
        {
            var repository = ApiTester.ServiceProvider.GetService<IConsortiumRepository>();
            await repository.SaveAsync(consortium);
        }

        public static async Task CreateFunctionalUnitInDatabase(string consortiumCode,
            FunctionalUnitCreateViewModel functionalUnit)
        {
            var repository = ApiTester.ServiceProvider.GetService<IFunctionalUnitRepository>();
            await repository.SaveAsync(consortiumCode, functionalUnit);
        }

        public static async Task CreateUserInDatabase(string consortiumCode, string functionalUnitCode, string userId,
            UserCreateViewModel user)
        {
            var repository = ApiTester.ServiceProvider.GetService<IUserRepository>();
            await repository.SaveAsync(consortiumCode, functionalUnitCode, userId, null, user);
        }

        public static async Task CreateAdministratorInDatabase(string administratorId,
            AdministratorCreateViewModel model)
        {
            var repository = ApiTester.ServiceProvider.GetService<IAdminRepository>();
            await repository.SaveAsync(model, administratorId, null);
        }

        public static async Task LinkAdministratorToConsortium(string consortiumCode, string administratorId)
        {
            var repository = ApiTester.ServiceProvider.GetService<IConsortiumRepository>();
            await repository.UpdateAdministratorIdAsync(administratorId, consortiumCode);
        }

        public static async Task<int> CreateFacilityInDatabase(string consortiumCode, FacilityCreateViewModel model)
        {
            var repository = ApiTester.ServiceProvider.GetService<IFacilityRepository>();
            var facility = await repository.SaveAsync(consortiumCode, model);

            return facility.Id;
        }

        public static async Task<int> CreateBookingInDatabase(string consortiumCode, BookingCreateViewModel model)
        {
            var repository = ApiTester.ServiceProvider.GetService<IBookingRepository>();
            return await repository.SaveAsync(consortiumCode, model);
        }
    }
}