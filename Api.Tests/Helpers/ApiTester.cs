﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using Xunit;

namespace Api.Tests.Helpers
{
    public static class ApiTester
    {
        private static readonly HttpClient Client;

        /// <summary>
        /// Configuration variables
        /// </summary>
        public static IConfiguration Configuration { get; }

        /// <summary>
        /// Authorization token
        /// </summary>
        public static AuthorizationToken AuthorizationToken { get; }

        /// <summary>
        /// Service provider
        /// </summary>
        public static IServiceProvider ServiceProvider { get; }

        static ApiTester()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(services => services.AddAutofac())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    config.AddEnvironmentVariables();
                    if (env.IsDevelopment())
                    {
                        config.AddUserSecrets<Startup>();
                    }
                })
                .UseSetting("UnitTesting", "true")
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();

                    if (hostingContext.HostingEnvironment.IsDevelopment())
                    {
                        logging.AddDebug();
                    }
                })
                .UseStartup<TestStartup>());
            

            Client = server.CreateClient();            
            Configuration = (IConfigurationRoot) server.Host.Services.GetService(typeof(IConfiguration));            
            AuthorizationToken = GetAuthorizationToken();
            ServiceProvider = server.Host.Services;
        }

        public static async Task Run(params Step[] steps)
        {
            int stepNumber = 1;

            foreach (var step in steps)
            {
                var f = new Func<HttpResponseMessage, bool>(r => r.IsSuccessStatusCode);
                var expectedStatusCodeMessage = "20x";

                if (string.IsNullOrEmpty(step.BuiltRelativeUri))
                {
                    throw new ArgumentNullException(
                        $"Error in step number {stepNumber}. Step.Uri cannot be null or empty.");
                }

                var request = step.BuildHttpRequestMessage();

                if (step.AuthenticationHeaderValue != null)
                {
                    request.Headers.Authorization = step.AuthenticationHeaderValue();
                }

                request.Headers.Host = request.RequestUri.Host;
                var response = await Client.SendAsync(request);

                if (step.ExpectedStatusCode.HasValue)
                {
                    var step1 = step;
                    f = (r => r.StatusCode == step1.ExpectedStatusCode);
                    expectedStatusCodeMessage = step.ExpectedStatusCode.ToString();
                }

                Assert.True(f(response),
                    $"Unexpected response status code in step number {stepNumber} (Expected: {expectedStatusCodeMessage} / Actual: {response.StatusCode}).");

                step.AssertResponse?.Invoke(response);

                if (step.Type != null && ((dynamic)step).AssertResponseContent != null)
                {
                    var result = response.Content.ReadAsAsync(step.Type).Result;
                    Assert.NotNull(result);

                    var action = ((dynamic)step).AssertResponseContent;
                    ((Delegate)action).DynamicInvoke(response, result);
                }
                stepNumber++;
            }
        }

        private static AuthorizationToken GetAuthorizationToken()
        {
            var parameters = new 
            {
                grant_type = "client_credentials",
                client_id = Configuration["Auth0:ClientId"],
                client_secret = Configuration["Auth0:ClientSecret"],
                audience = Configuration["Auth0:ApiIdentifier"]
            };

            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);

            var client = new RestClient($"https://{Configuration["Auth0:Domain"]}/oauth/token");            
            var response = client.Execute(request);
            dynamic content = JsonConvert.DeserializeObject(response.Content);

            return new AuthorizationToken
            {
                AccesToken = content.access_token,
                TokenType = content.token_type
            };
        }        
    }

    public class AuthorizationToken
    {
        public string AccesToken { get; set; }
        public string TokenType { get; set; }

        public override string ToString()
        {
            return $"{TokenType} {AccesToken}";
        }

        public AuthenticationHeaderValue ToAuthHeaderValue()
        {
            return new AuthenticationHeaderValue(TokenType, AccesToken);
        }
    }

    public class Post<TContent, TResponse> : Post<TContent>
    {
        internal override Type Type => typeof(TResponse);
        public Action<HttpResponseMessage, TResponse> AssertResponseContent { get; set; }
    }

    public class Post<TContent> : Step
    {
        private readonly Lazy<TContent> _builtContent;
        public Func<TContent> Content { get; set; }

        public Post()
        {
            _builtContent = new Lazy<TContent>(() => Content());
        }

        public override HttpRequestMessage BuildHttpRequestMessage()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(UriBase, BuiltRelativeUri))
            {
                Content = new ObjectContent<TContent>(_builtContent.Value, new JsonMediaTypeFormatter())
            };

            return request;
        }
    }

    public class Get<TResponse> : Get
    {
        internal override Type Type => typeof(TResponse);

        public Action<HttpResponseMessage, TResponse> AssertResponseContent { get; set; }
    }

    public class Get : Step
    {
        public override HttpRequestMessage BuildHttpRequestMessage()
        {
            return new HttpRequestMessage(HttpMethod.Get, new Uri(UriBase, BuiltRelativeUri));
        }
    }

    public abstract class Step
    {
        protected static readonly Uri UriBase = new Uri("http://localhost");
        private readonly Lazy<string> _builtRelativeUri;

        protected Step()
        {
            _builtRelativeUri = new Lazy<string>(() => Uri());
        }

        internal virtual Type Type => null;

        public Func<string> Uri { get; set; }
        internal string BuiltRelativeUri => _builtRelativeUri.Value;
        public HttpStatusCode? ExpectedStatusCode { get; set; }
        public Action<HttpResponseMessage> AssertResponse { get; set; }

        public Func<AuthenticationHeaderValue> AuthenticationHeaderValue { get; set; }
        public abstract HttpRequestMessage BuildHttpRequestMessage();
    }

    public class Put<TContent, TResponse> : Put<TContent>
    {
        internal override Type Type => typeof(TResponse);
        public Action<HttpResponseMessage, TResponse> AssertResponseContent { get; set; }
    }

    public class Put<TContent> : Step
    {
        private readonly Lazy<TContent> _builtContent;
        public Func<TContent> Content { get; set; }

        public Put()
        {
            _builtContent = new Lazy<TContent>(() => Content());
        }

        public override HttpRequestMessage BuildHttpRequestMessage()
        {
            var request = new HttpRequestMessage(HttpMethod.Put, new Uri(UriBase, BuiltRelativeUri))
            {
                Content = new ObjectContent<TContent>(_builtContent.Value, new JsonMediaTypeFormatter())               
            };

            return request;
        }
    }

    public class Delete<TResponse> : Delete
    {
        internal override Type Type => typeof(TResponse);

        public Action<HttpResponseMessage, TResponse> AssertResponseContent { get; set; }
    }

    public class Delete : Step
    {
        public override HttpRequestMessage BuildHttpRequestMessage()
        {
            return new HttpRequestMessage(HttpMethod.Delete, new Uri(UriBase, BuiltRelativeUri));
        }
    }
}