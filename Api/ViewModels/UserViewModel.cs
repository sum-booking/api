﻿using Api.Models;

namespace Api.ViewModels
{
    /// <inheritdoc />
    public class UserViewModel : User
    {
        /// <summary>
        /// Functiona unit name
        /// </summary>
        public string FunctionalUnit { get; set; }

        /// <summary>
        /// Consortium name
        /// </summary>
        public string Consortium { get; set; }
    }
}