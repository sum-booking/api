﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api.ViewModels
{
    /// <summary>
    /// Administrator create view model
    /// </summary>
    public class AdministratorCreateViewModel : CreateBaseViewModel
    {
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        [Required]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Consortium codes
        /// </summary>
        [Required]
        public List<string> ConsortiumCodes { get; set; }

    }
}