﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Api.Models;

namespace Api.ViewModels
{
    /// <summary>
    /// Administrator
    /// </summary>
    public class AdministratorViewModel : Administrator
    {
        /// <summary>
        /// Consortium codes
        /// </summary>
        [Required]
        public List<Consortium> Consortiums { get; set; }
    }
}