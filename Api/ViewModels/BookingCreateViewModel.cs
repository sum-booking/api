﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.ViewModels
{
    /// <summary>
    /// Booking
    /// </summary>
    public class BookingCreateViewModel : CreateBaseViewModel
    {
        /// <summary>
        /// Functional unit code
        /// </summary>
        [Required]
        public string FunctionalUnitCode { get; set; }

        /// <summary>
        /// Facility id
        /// </summary>
        [Required]
        public int FacilityId { get; set; }

        /// <summary>
        /// Book date
        /// </summary>
        public DateTimeOffset BookDate { get; set; }

        /// <summary>
        /// Is mid day booking
        /// </summary>
        public bool? IsMidDay { get; set; }

        /// <summary>
        /// Is night booking
        /// </summary>
        public bool? IsNight { get; set; }
    }
}