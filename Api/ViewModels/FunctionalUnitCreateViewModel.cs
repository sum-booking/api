﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.ViewModels
{
    /// <summary>
    /// Functional unit creation model
    /// </summary>
    public class FunctionalUnitCreateViewModel : CreateBaseViewModel
    {
        /// <summary>
        /// Designation
        /// </summary>
        [Required]
        public string Designation { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}