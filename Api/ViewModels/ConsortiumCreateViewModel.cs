﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.ViewModels
{
    /// <summary>
    /// Consortium creation model
    /// </summary>
    public class ConsortiumCreateViewModel : CreateBaseViewModel
    {
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}