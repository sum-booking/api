﻿using System.ComponentModel.DataAnnotations;

namespace Api.ViewModels
{
    /// <summary>
    /// User create view model
    /// </summary>
    public class UserCreateViewModel : CreateBaseViewModel
    {
        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        [Required]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Send authentication link
        /// </summary>
        public bool SendWelcomingMail { get; set; }

        /// <summary>
        /// Phone notifications
        /// </summary>
        public bool PhoneNotifications { get; set; }

        /// <summary>
        /// Email notifications
        /// </summary>
        public bool EmailNotifications { get; set; }

    }
}