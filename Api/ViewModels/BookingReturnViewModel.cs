﻿namespace Api.ViewModels
{
    /// <summary>
    /// Booking return view model
    /// </summary>
    public class BookingReturnViewModel : BookingCreateViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Consortium Id
        /// </summary>
        public int ConsortiumId { get; set; }

        /// <summary>
        /// Facility name
        /// </summary>
        public string FacilityName { get; set; }

        /// <summary>
        /// Functional unit designation
        /// </summary>
        public string FunctionalUnitDesignation { get; set; }
    }
}