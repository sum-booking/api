﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.ViewModels
{
    /// <summary>
    /// Create base view model
    /// </summary>
    public abstract class CreateBaseViewModel
    {
        /// <summary>
        /// Created by
        /// </summary>
        [Required]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Created on with timezone
        /// </summary>
        [Required]
        public DateTimeOffset CreatedOn { get; set; }
    }
}