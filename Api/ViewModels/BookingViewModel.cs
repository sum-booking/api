﻿using Api.Models;

namespace Api.ViewModels
{
    /// <inheritdoc />
    public class BookingViewModel : Booking
    {
        /// <summary>
        /// Facility name
        /// </summary>
        public string FacilityName { get; set; }

        /// <summary>
        /// Functional unit designation
        /// </summary>
        public string FunctionalUnitDesignation { get; set; }
    }
}