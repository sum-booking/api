﻿using System;
using System.Collections.Generic;
using System.IO;
using Api.Migrations;
using Api.Plumbing;
using Api.Plumbing.Policies;
using Api.Repositories;
using Api.Services;
using Api.Services.Limits;
using Autofac;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json.Converters;
using SentryDotNet;
using SentryDotNet.AspNetCore;
using Swashbuckle.AspNetCore.Swagger;

namespace Api
{
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup
    {
        private readonly IHostingEnvironment _environment;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="environment"></param>
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            _environment = environment;
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Configure services
        /// </summary>
        /// <param name="services"></param>
        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            ConfigureMvc(services);

            services.AddApiVersioning(o => o.ReportApiVersions = true);

            ConfigureJwtAuthentication(services);

            ConfigureSwagger(services);            

            services.AddSentryDotNet(new SentryClient(Configuration["Sentry:DSN"], new SentryEventDefaults(
                environment: _environment.EnvironmentName,
                release: typeof(Startup).Assembly.GetName().Version.ToString(3),
                logger: _environment.ApplicationName)));
        }

        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="provider"></param>
        /// <param name="sentryClient"></param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IApiVersionDescriptionProvider provider, ISentryClient sentryClient)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePages();
                app.UseSentryDotNet(new SentryDotNetOptions { CaptureRequestBody = true });
            }

            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseMvc();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "docs/{documentName}/swagger.json";
            });

            ConfigureSwaggerUI(app, provider);

            UpgradeDatabase.Upgrade(
                Configuration.GetConnectionString("ApiDatabase"),
                Convert.ToBoolean(Configuration["UnitTesting"]),
                sentryClient
                );
        }

        /// <summary>
        /// Configure Autofac container
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new AutofacModule());
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

                // add a swagger document for each discovered API version
                // note: you might choose to skip or document deprecated API versions differently
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                }

                // add a custom operation filter which sets default values
                c.OperationFilter<SwaggerDefaultValues>();
                c.DescribeAllEnumsAsStrings();
                c.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    AuthorizationUrl = $"https://{Configuration["Auth0:Domain"]}/authorize",
                    Flow = "implicit",
                    Scopes = new Dictionary<string, string>
                    {
                        {"openid", "Read OIDC data"}
                    }
                });

                var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Api.xml");
                c.IncludeXmlComments(filePath);
            });
        }

        private void ConfigureJwtAuthentication(IServiceCollection services)
        {
            string domain = $"https://{Configuration["Auth0:Domain"]}/";
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = domain;
                options.Audience = Configuration["Auth0:ApiIdentifier"];
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Root", policy => policy.AddRequirements(new RootRequirement(domain)));
                options.AddPolicy("ConsortiumAdmin", policy => policy.AddRequirements(new ConsortiumAdminRequirement(domain)));
                options.AddPolicy("FunctionalUnitUser",
                    policy => policy.AddRequirements(new FunctionalUnitUserRequirement(domain)));
            });

            services.AddSingleton<IAuthorizationHandler, RootAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, ConsortiumAdminAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, FunctionalUnitUserAuthorizationHandler>();
        }

        private static void ConfigureMvc(IServiceCollection services)
        {
            var mvcBuilder = services.AddMvcCore();
            mvcBuilder.AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");
            mvcBuilder.AddApiExplorer();
            mvcBuilder.AddAuthorization();
            mvcBuilder.AddCacheTagHelper();
            mvcBuilder.AddDataAnnotations();
            mvcBuilder.AddJsonFormatters();
            mvcBuilder.AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new StringEnumConverter());
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", p => p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().WithExposedHeaders("Location"));
            });
        }

        private void ConfigureSwaggerUI(IApplicationBuilder app, IApiVersionDescriptionProvider provider)
        {
            app.UseSwaggerUI(c =>
            {
                // build a swagger endpoint for each discovered API version
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerEndpoint($"/docs/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                }
                c.RoutePrefix = "docs";
                c.ConfigureOAuth2(Configuration["Auth0:ClientId"], null, null, null, " ",
                    new { audience = Configuration["Auth0:ApiIdentifier"] });
            });
        }

        private static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new Info
            {
                Title = $"Booking API {description.ApiVersion}",
                Version = description.ApiVersion.ToString(),
                Description = "Booking API",
                Contact = new Contact { Name = "Ignacio Glinsek", Email = "iglinsek@gmail.com" },
                TermsOfService = "Copyright (c) 2018",
            };

            if (description.IsDeprecated)
            {
                info.Description += " This API version has been deprecated.";
            }

            return info;
        }
    }
}
