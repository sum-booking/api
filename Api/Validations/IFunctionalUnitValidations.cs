﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Validations
{
    /// <summary>
    /// Functional unit validations
    /// </summary>
    public interface IFunctionalUnitValidations
    {
        /// <summary>
        /// Validate references
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<IDictionary<string, string>> ValidateReferences(FunctionalUnitCreateViewModel model, string consortiumCode);

        /// <summary>
        /// Validate existance
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <returns></returns>
        Task<KeyValuePair<string, string>?> ValidateExistance(string consortiumCode, string functionalUnitCode);
    }
}