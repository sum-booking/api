﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Validations
{
    /// <inheritdoc />
    public class BookingValidations : IBookingValidations
    {
        private readonly IConsortiumValidations _consortiumValidations;
        private readonly IFunctionalUnitValidations _functionalUnitValidations;
        private readonly IFacilityValidations _facilityValidations;

        /// <inheritdoc />
        public BookingValidations(IConsortiumValidations consortiumValidations, IFunctionalUnitValidations functionalUnitValidations,
            IFacilityValidations facilityValidations)
        {
            _consortiumValidations = consortiumValidations;
            _functionalUnitValidations = functionalUnitValidations;
            _facilityValidations = facilityValidations;
        }

        /// <summary>
        /// Validate create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string, string> ValidateCreate(BookingCreateViewModel model)
        {
            var errors = model.ValidateCreate();

            if (model.BookDate == DateTimeOffset.MinValue)
            {
                errors.Add("BookDate", "Is required");
            }

            return errors;
        }


        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateReferences(BookingCreateViewModel model,
            string consortiumCode)
        {
            var errors = ValidateCreate(model);
            var notExistsConsortium = await _consortiumValidations.ValidateExistance(consortiumCode);
            if (notExistsConsortium != null)
            {
                errors.Add(notExistsConsortium.Value);
            }

            var noExistsFunctionalUnit = await _functionalUnitValidations.ValidateExistance(consortiumCode, model.FunctionalUnitCode);
            if (noExistsFunctionalUnit != null)
            {
                errors.Add(noExistsFunctionalUnit.Value);
            }

            var noExistsFacility = await _facilityValidations.ValidateExistance(consortiumCode, model.FacilityId);
            if (noExistsFacility != null)
            {
                errors.Add(noExistsFacility.Value);
            }

            return errors;
        }        
    }
}