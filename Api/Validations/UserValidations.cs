﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.Repositories;
using Api.ViewModels;
using Dapper;

namespace Api.Validations
{
    /// <inheritdoc />
    public class UserValidations : IUserValidations
    {
        private readonly IConsortiumValidations _consortiumValidations;
        private readonly IFunctionalUnitValidations _functionalUnitValidations;

        /// <inheritdoc />
        public UserValidations(IConsortiumValidations consortiumValidations, IFunctionalUnitValidations functionalUnitValidations)
        {
            _consortiumValidations = consortiumValidations;
            _functionalUnitValidations = functionalUnitValidations;
        }

        /// <summary>
        /// Validate create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string, string> ValidateCreate(UserCreateViewModel model)
        {
            var errors = model.ValidateCreate();

            if (String.IsNullOrWhiteSpace(model.Email))
            {
                errors.Add("Email", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.LastName))
            {
                errors.Add("LastName", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.FirstName))
            {
                errors.Add("FirstName", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.PhoneNumber))
            {
                errors.Add("PhoneNumber", "Is required");
            }

            return errors;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateReferences(UserCreateViewModel model,
            string consortiumCode, string functionalUnitCode)
        {
            var errors = ValidateCreate(model);
            var notExistsConsortium = await _consortiumValidations.ValidateExistance(consortiumCode);
            if (notExistsConsortium != null)
            {
                errors.Add(notExistsConsortium.Value);
            }

            var notExistsFunctionalUnit = await _functionalUnitValidations.ValidateExistance(consortiumCode, functionalUnitCode);
            if (notExistsFunctionalUnit != null)
            {
                errors.Add(notExistsFunctionalUnit.Value);
            }

            return errors;
        }        
    }
}