﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.ViewModels;
using Dapper;

namespace Api.Validations
{
    /// <inheritdoc />
    public class AdministratorValidations : IAdministratorValidations
    {
        private readonly IConsortiumValidations _consortiumValidations;

        /// <inheritdoc />
        public AdministratorValidations(IConsortiumValidations consortiumValidations)
        {
            _consortiumValidations = consortiumValidations;
        }

        /// <summary>
        /// Validate create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string, string> ValidateCreate(AdministratorCreateViewModel model)
        {
            var errors = model.ValidateCreate();

            if (String.IsNullOrWhiteSpace(model.Email))
            {
                errors.Add("Email", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.LastName))
            {
                errors.Add("LastName", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.FirstName))
            {
                errors.Add("FirstName", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.PhoneNumber))
            {
                errors.Add("PhoneNumber", "Is required");
            }

            if (model.ConsortiumCodes == null || model.ConsortiumCodes.Count == 0)
            {
                errors.Add("ConsortiumCodes", "Is Required");
            }

            return errors;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateReferences(AdministratorCreateViewModel model)
        {
            var errors = ValidateCreate(model);
            if (model.ConsortiumCodes != null)
            {
                foreach (var consortiumCode in model.ConsortiumCodes)
                {
                    var notExistsConsortium = await _consortiumValidations.ValidateExistance(consortiumCode);
                    if (notExistsConsortium != null)
                    {
                        errors.Add(notExistsConsortium.Value);
                        break;
                    }
                }
            }

            return errors;
        }
    }
}