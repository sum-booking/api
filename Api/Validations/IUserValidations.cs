﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Validations
{
    /// <summary>
    /// User validations
    /// </summary>
    public interface IUserValidations
    {
        /// <summary>
        /// Validate references
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <returns></returns>
        Task<IDictionary<string, string>> ValidateReferences(UserCreateViewModel model, string consortiumCode, 
            string functionalUnitCode);
    }
}