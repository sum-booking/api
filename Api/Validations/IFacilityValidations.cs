﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Validations
{
    /// <summary>
    /// Facility validations
    /// </summary>
    public interface IFacilityValidations
    {
        /// <summary>
        /// Validate references
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<IDictionary<string, string>> ValidateReferences(FacilityCreateViewModel model, string consortiumCode);

        /// <summary>
        /// Validate existance
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="facilityId"></param>
        /// <returns></returns>
        Task<KeyValuePair<string, string>?> ValidateExistance(string consortiumCode, int facilityId);
    }
}