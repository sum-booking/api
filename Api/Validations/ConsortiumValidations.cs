﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Validations
{
    /// <inheritdoc />
    public class ConsortiumValidations : IConsortiumValidations
    {
        private readonly IConsortiumRepository _consortiumRepository;

        /// <inheritdoc />
        public ConsortiumValidations(IConsortiumRepository consortiumRepository)
        {
            _consortiumRepository = consortiumRepository;
        }

        /// <summary>
        /// Validate create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string, string> ValidateCreate(ConsortiumCreateViewModel model)
        {
            var errors = model.ValidateCreate();

            if (String.IsNullOrWhiteSpace(model.Name))
            {
                errors.Add("Name", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.Code))
            {
                errors.Add("Code", "Is required");
            } else if (model.Code.Length > 8)
            {
                errors.Add("Code", "Must be less than 8 chars");
            }

            return errors;
        }

        /// <inheritdoc />
        public async Task<KeyValuePair<string,string>?> ValidateExistance(string consortiumCode)
        {
            var existsConsortium = await _consortiumRepository.ExistsAsync(consortiumCode);

            if (!existsConsortium)
            {
                return new KeyValuePair<string, string>("ConsortiumCode", "Must be valid");
            }

            return null;
        }
    }
}