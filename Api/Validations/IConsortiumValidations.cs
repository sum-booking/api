﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Validations
{
    /// <summary>
    /// Consortium validations
    /// </summary>
    public interface IConsortiumValidations
    {
        /// <summary>
        /// Validate existance
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<KeyValuePair<string, string>?> ValidateExistance(string consortiumCode);
    }
}