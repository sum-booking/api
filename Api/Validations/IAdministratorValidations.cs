﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Validations
{
    /// <summary>
    /// User validations
    /// </summary>
    public interface IAdministratorValidations
    {
        /// <summary>
        /// Validate references
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<IDictionary<string, string>> ValidateReferences(AdministratorCreateViewModel model);
    }
}