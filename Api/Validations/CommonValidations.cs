﻿using System;
using System.Collections.Generic;
using Api.ViewModels;

namespace Api.Validations
{
    /// <summary>
    /// Common validations
    /// </summary>
    public static class CommonValidations
    {
        /// <summary>
        /// Validate create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string, string> ValidateCreate(this CreateBaseViewModel model)
        {
            var errors = new Dictionary<string, string>();

            if (model.CreatedOn <= DateTimeOffset.MinValue)
            {
                errors.Add("CreatedOn", "Must be a valid date");
            }

            if (String.IsNullOrWhiteSpace(model.CreatedBy))
            {
                errors.Add("CreatedBy", "Is required");
            }

            return errors;
        }
    }
}