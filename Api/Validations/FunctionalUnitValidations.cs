﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.Repositories;
using Api.ViewModels;
using Dapper;

namespace Api.Validations
{
    /// <inheritdoc />
    public class FunctionalUnitValidations : IFunctionalUnitValidations
    {
        private readonly IConsortiumValidations _consortiumValidations;
        private readonly IFunctionalUnitRepository _functionalUnitRepository;

        /// <inheritdoc />
        public FunctionalUnitValidations(IConsortiumValidations consortiumValidations, IFunctionalUnitRepository functionalUnitRepository)
        {
            _consortiumValidations = consortiumValidations;
            _functionalUnitRepository = functionalUnitRepository;
        }

        /// <summary>
        /// Validate create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string, string> ValidateCreate(FunctionalUnitCreateViewModel model)
        {
            var errors = model.ValidateCreate();

            if (String.IsNullOrWhiteSpace(model.Designation))
            {
                errors.Add("Designation", "Is required");
            }

            if (String.IsNullOrWhiteSpace(model.Code))
            {
                errors.Add("Code", "Is required");
            }
            else if (model.Code.Length > 8)
            {
                errors.Add("Code", "Must be less than 8 chars");
            }

            return errors;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateReferences(FunctionalUnitCreateViewModel model,
            string consortiumCode)
        {
            var errors = ValidateCreate(model);
            var notExistsConsortium = await _consortiumValidations.ValidateExistance(consortiumCode);
            if (notExistsConsortium != null)
            {
                errors.Add(notExistsConsortium.Value);
            }

            return errors;
        }

        /// <inheritdoc />
        public async Task<KeyValuePair<string, string>?> ValidateExistance(string consortiumCode, string functionalUnitCode)
        {
            var existsFunctionalUnit = await _functionalUnitRepository.ExistsAsync(consortiumCode, functionalUnitCode);

            if (!existsFunctionalUnit)
            {
                return new KeyValuePair<string, string>("FunctionalUnitCode", "Must be valid");
            }

            return null;
        }
    }
}