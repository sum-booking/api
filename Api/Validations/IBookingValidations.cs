﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Validations
{
    /// <summary>
    /// Booking validations
    /// </summary>
    public interface IBookingValidations
    {
        /// <summary>
        /// Validate references
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<IDictionary<string, string>> ValidateReferences(BookingCreateViewModel model, string consortiumCode);
    }
}