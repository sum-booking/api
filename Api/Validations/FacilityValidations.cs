﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Validations
{
    /// <inheritdoc />
    public class FacilityValidations : IFacilityValidations
    {
        private readonly IConsortiumValidations _consortiumValidations;
        private readonly IFacilityRepository _facilityRepository;

        /// <inheritdoc />
        public FacilityValidations(IConsortiumValidations consortiumValidations, IFacilityRepository facilityRepository)
        {
            _consortiumValidations = consortiumValidations;
            _facilityRepository = facilityRepository;
        }

        /// <summary>
        /// Validate create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string, string> ValidateCreate(FacilityCreateViewModel model)
        {
            var errors = model.ValidateCreate();

            if (String.IsNullOrWhiteSpace(model.Name))
            {
                errors.Add("Name", "Is required");
            }

            return errors;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateReferences(FacilityCreateViewModel model,
            string consortiumCode)
        {
            var errors = ValidateCreate(model);
            var notExistsConsortium = await _consortiumValidations.ValidateExistance(consortiumCode);
            if (notExistsConsortium != null)
            {
                errors.Add(notExistsConsortium.Value);
            }

            return errors;
        }

        /// <inheritdoc />
        public async Task<KeyValuePair<string, string>?> ValidateExistance(string consortiumCode, int facilityId)
        {
            var existsFacility = await _facilityRepository.ExistsAsync(consortiumCode, facilityId);

            if (!existsFacility)
            {
                return new KeyValuePair<string, string>("FacilityId", "Must be valid");
            }

            return null;
        }
    }
}