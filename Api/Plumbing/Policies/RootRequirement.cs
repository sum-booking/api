﻿namespace Api.Plumbing.Policies
{
    /// <inheritdoc />
    public class RootRequirement : RequirementBase
    {
        /// <inheritdoc />
        public RootRequirement(string issuer) : base(issuer)
        {
        }
    }
}