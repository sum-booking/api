﻿using System;
using Microsoft.AspNetCore.Authorization;

namespace Api.Plumbing.Policies
{
    /// <inheritdoc />
    public class ConsortiumAdminRequirement : RequirementBase
    {
        /// <inheritdoc />
        public ConsortiumAdminRequirement(string issuer) : base(issuer)
        {
        }
    }
}