﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace Api.Plumbing.Policies
{
    /// <summary>
    /// Policies helpers
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// Check if claim matches value
        /// </summary>
        /// <param name="user"></param>
        /// <param name="issuer"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ClaimMatches(this ClaimsPrincipal user, string issuer, string type, string value)
        {
            if (!user.HasClaim(c => c.Type == type && c.Issuer == issuer)) return false;

            var obtainedValue = user.FindFirst(c => c.Type == type && c.Issuer == issuer).Value;

            return obtainedValue == value;

        }

        /// <summary>
        /// Check Root Requirement
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        public static bool CheckRootRequirement(AuthorizationHandlerContext context, RequirementBase requirement)
        {
            const string type = "https://sumbooking.com/isRoot";
            var matches = context.User.ClaimMatches(requirement.Issuer, type, "true");
            return matches;
        }

        /// <summary>
        /// Check consortium admin requirement
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        public static bool CheckConsortiumAdminRequirement(AuthorizationHandlerContext context,
            RequirementBase requirement)
        {
            const string isConsortiumAdminType = "https://sumbooking.com/consortiumAdmin";
            const string consortiumCodesType = "https://sumbooking.com/consortiumCodes";
            var isAdmin = context.User.ClaimMatches(requirement.Issuer, isConsortiumAdminType, "true");
            if (isAdmin)
            {
                var filterContext = (FilterContext)context.Resource;
                var consortiumCode = filterContext.RouteData.Values.ContainsKey("consortiumCode")
                    ? filterContext.RouteData.Values["consortiumCode"].ToString()
                    : null;
                if (consortiumCode != null)
                {
                    var claims = context.User.Claims.Where(x =>
                        x.Issuer == requirement.Issuer && x.Type == consortiumCodesType);

                    if (claims.Any())
                    {
                        return claims.Any(x => x.Value == consortiumCode);
                    }
                    return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check functional unit user requirement
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        public static bool CheckFunctionalUnitUserRequirement(AuthorizationHandlerContext context,
            RequirementBase requirement)
        {
            const string consortiumCodeType = "https://sumbooking.com/consortiumCode";
            const string functionalUnitCodeType = "https://sumbooking.com/functionalUnitCode";
            var filterContext = (FilterContext)context.Resource;
            var functionalUnitCode = filterContext.RouteData.Values.ContainsKey("functionalUnitCode")
                ? filterContext.RouteData.Values["functionalUnitCode"].ToString()
                : null;
            var matchesFunctionalUnitClaim = functionalUnitCode == null || context.User.ClaimMatches(requirement.Issuer, functionalUnitCodeType, functionalUnitCode);
            var consortiumCode = filterContext.RouteData.Values.ContainsKey("consortiumCode")
                ? filterContext.RouteData.Values["consortiumCode"].ToString()
                : null;

            return matchesFunctionalUnitClaim &&
                   context.User.ClaimMatches(requirement.Issuer, consortiumCodeType, consortiumCode);
        }
    }
}