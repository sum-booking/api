﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Api.Plumbing.Policies
{
    /// <inheritdoc />
    public class ConsortiumAdminAuthorizationHandler : AuthorizationHandler<ConsortiumAdminRequirement>
    {
        /// <inheritdoc />
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ConsortiumAdminRequirement requirement)
        {
            var matchesRoot = Helpers.CheckRootRequirement(context, requirement);
            if (matchesRoot)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            var matchesConsortiumCode = Helpers.CheckConsortiumAdminRequirement(context, requirement);
            if (matchesConsortiumCode)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}