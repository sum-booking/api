using System;
using Microsoft.AspNetCore.Authorization;

namespace Api.Plumbing.Policies
{
    /// <inheritdoc />
    public abstract class RequirementBase : IAuthorizationRequirement
    {
        /// <summary>
        /// Issuer
        /// </summary>
        public string Issuer { get; }

        /// <inheritdoc />
        protected RequirementBase(string issuer)
        {
            Issuer = issuer ?? throw new ArgumentNullException(nameof(issuer));
        }
    }
}