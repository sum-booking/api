﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Api.Plumbing.Policies
{
    /// <inheritdoc />
    public class FunctionalUnitUserAuthorizationHandler : AuthorizationHandler<FunctionalUnitUserRequirement>
    {
        /// <inheritdoc />
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, FunctionalUnitUserRequirement requirement)
        {
            var matchesRoot = Helpers.CheckRootRequirement(context, requirement);
            if (matchesRoot)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            var matchesConsortiumCode = Helpers.CheckConsortiumAdminRequirement(context, requirement);
            if (matchesConsortiumCode)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            var matchesFunctionalUnit = Helpers.CheckFunctionalUnitUserRequirement(context, requirement);
            if (matchesFunctionalUnit)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}