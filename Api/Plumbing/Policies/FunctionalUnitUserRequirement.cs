﻿namespace Api.Plumbing.Policies
{
    /// <inheritdoc />
    public class FunctionalUnitUserRequirement : RequirementBase
    {
        /// <inheritdoc />
        public FunctionalUnitUserRequirement(string issuer):base(issuer)
        {
        }
    }
}