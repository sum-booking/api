﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Api.Plumbing.Policies
{
    /// <inheritdoc />
    public class RootAuthorizationHandler : AuthorizationHandler<RootRequirement>
    {
        /// <inheritdoc />
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RootRequirement requirement)
        {
            var matches = Helpers.CheckRootRequirement(context, requirement);
            if (matches)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }        
    }
}