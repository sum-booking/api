﻿using System;
using System.Linq;
using System.Reflection;
using Api.Models.Enums;
using Api.Services.AnticipationPolicies;
using Api.Services.CancellationPolicies;
using Api.Services.Schedules;
using Autofac;
using Module = Autofac.Module;

namespace Api.Plumbing
{
    /// <inheritdoc />
    public class AutofacModule : Module
    {
        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            var dataAccess = Assembly.GetAssembly(GetType());

            // Register repositories
            builder.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            // Register common services
            builder.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Namespace == "Api.Services")
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            //Register limits
            builder.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Namespace == "Api.Services.Limits")
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            //Register validations
            builder.RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Namespace == "Api.Validations")
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            RegisterScheduleStrategies(builder, dataAccess);

            RegisterCancellationStrategies(builder, dataAccess);

            RegisterAnticipationStrategies(builder, dataAccess);
        }

        private static void RegisterCancellationStrategies(ContainerBuilder builder, Assembly dataAccess)
        {
            //Register cancellation strategies
            builder.RegisterType<CancellationPolicyFactory>()
                .AsImplementedInterfaces().InstancePerLifetimeScope();
            var strategies = dataAccess.GetTypes().Where(type => type.IsClass && type.IsAssignableTo<ICancellationPolicy>());
            foreach (var strategy in strategies)
            {
                var key = strategy.GetCustomAttribute<CancellationPolicyKeyAttribute>();
                builder.RegisterType(strategy)
                    .Keyed<ICancellationPolicy>(key.Value).InstancePerLifetimeScope();
            }

            // Register cancellation strategy autofac factory given a key
            builder.Register<Func<BookingCancellationPolicy, ICancellationPolicy>>(c =>
            {
                var context = c.Resolve<IComponentContext>();
                return strategy => context.ResolveKeyed<ICancellationPolicy>(strategy);
            }).InstancePerLifetimeScope();
        }

        private static void RegisterScheduleStrategies(ContainerBuilder builder, Assembly dataAccess)
        {
            //Register schedule strategies
            builder.RegisterType<SchedulingStrategyFactory>()
                .AsImplementedInterfaces().InstancePerLifetimeScope();
            var strategies = dataAccess.GetTypes().Where(type => type.IsClass && type.IsAssignableTo<ISchedulingStrategy>());
            foreach (var strategy in strategies)
            {
                var key = strategy.GetCustomAttribute<ScheduleStrategyKeyAttribute>();
                builder.RegisterType(strategy)
                    .Keyed<ISchedulingStrategy>(key.Value).InstancePerLifetimeScope();
            }

            // Register schedule strategy autofac factory given a key
            builder.Register<Func<BookingScheduleStrategy, ISchedulingStrategy>>(c =>
            {
                var context = c.Resolve<IComponentContext>();
                return strategy => context.ResolveKeyed<ISchedulingStrategy>(strategy);
            }).InstancePerLifetimeScope();
        }

        private static void RegisterAnticipationStrategies(ContainerBuilder builder, Assembly dataAccess)
        {
            //Register schedule strategies
            builder.RegisterType<AnticipationPolicyFactory>()
                .AsImplementedInterfaces().InstancePerLifetimeScope();
            var strategies = dataAccess.GetTypes().Where(type => type.IsClass && type.IsAssignableTo<IBookingAnticipationPolicy>());
            foreach (var strategy in strategies)
            {
                var key = strategy.GetCustomAttribute<AnticipationPolicyKeyAttribute>();
                builder.RegisterType(strategy)
                    .Keyed<IBookingAnticipationPolicy>(key.Value).InstancePerLifetimeScope();
            }

            // Register schedule strategy autofac factory given a key
            builder.Register<Func<BookingAnticipationPolicy, IBookingAnticipationPolicy>>(c =>
            {
                var context = c.Resolve<IComponentContext>();
                return strategy => context.ResolveKeyed<IBookingAnticipationPolicy>(strategy);
            }).InstancePerLifetimeScope();
        }
    }
}