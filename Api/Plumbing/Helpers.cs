﻿using System;

namespace Api.Plumbing
{
    /// <summary>
    /// Helpers
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// Trim datetime offset
        /// </summary>
        /// <param name="date"></param>
        /// <param name="roundTicks"></param>
        /// <returns></returns>
        public static DateTimeOffset Trim(this DateTimeOffset date, long roundTicks)
        {
            return new DateTimeOffset(date.Ticks - date.Ticks % roundTicks, date.Offset);
        }
    }
}