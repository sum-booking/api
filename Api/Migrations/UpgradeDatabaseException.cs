﻿using System;

namespace Api.Migrations
{
    /// <inheritdoc />
    public class UpgradeDatabaseException : Exception
    {
        /// <inheritdoc />
        public UpgradeDatabaseException(Exception resultError) : base("Upgrade exception", resultError)
        {

        }

        /// <inheritdoc />
        public UpgradeDatabaseException(string resultError) : base(resultError)
        {

        }
    }
}