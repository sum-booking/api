﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using DbUp;
using Npgsql;
using SentryDotNet;

namespace Api.Migrations
{
    /// <summary>
    /// Upgrade database
    /// </summary>
    public class UpgradeDatabase
    {
        /// <summary>
        /// Upgrade
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="shouldDropDb"></param>
        /// <param name="sentryClient"></param>
        public static void Upgrade(string connectionString, bool shouldDropDb, ISentryClient sentryClient)
        {
            var upgradeEngine = DeployChanges.To
                .PostgresqlDatabase(connectionString)
                .WithScriptsAndCodeEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .LogToTrace()
                .LogToConsole()
                .Build();

            if (shouldDropDb)
            {
                try
                {
                    DropDatabaseIfExists(connectionString);
                    EnsureDatabase.For.PostgresqlDatabase(connectionString);
                }
                catch (Exception ex)
                {
                    var sentryEventBuilder = sentryClient.CreateEventBuilder();
                    sentryEventBuilder.SetMessage("Could not drop and create database");
                    sentryEventBuilder.SetException(ex);
                    sentryClient.SendAsync(sentryEventBuilder.Build()).Wait();
                    throw new UpgradeDatabaseException(ex);
                }
            }

            if (upgradeEngine.TryConnect(out var dbError))
            {
                if (upgradeEngine.IsUpgradeRequired())
                {
                    var result = upgradeEngine.PerformUpgrade();
                    if (!result.Successful)
                    {
                        var sentryEventBuilder = sentryClient.CreateEventBuilder();
                        sentryEventBuilder.SetMessage($"Could not upgrade the database: {result.Error}");
                        sentryClient.SendAsync(sentryEventBuilder.Build()).Wait();
                        throw new UpgradeDatabaseException(result.Error);
                    }
                }
            }
            else
            {
                var sentryEventBuilder = sentryClient.CreateEventBuilder();
                sentryEventBuilder.SetMessage($"Could not connect to database: {dbError}");
                sentryClient.SendAsync(sentryEventBuilder.Build()).Wait();
                throw new UpgradeDatabaseException(dbError);
            }
        }

        private static void DropDatabaseIfExists(string connectionString)
        {
            var masterConnectionStringBuilder = new NpgsqlConnectionStringBuilder(connectionString);

            var databaseName = masterConnectionStringBuilder.Database;

            if (string.IsNullOrEmpty(databaseName) || databaseName.Trim() == string.Empty)
            {
                throw new InvalidOperationException("The connection string does not specify a database name.");
            }

            masterConnectionStringBuilder.Database = "postgres";

            using (var connection = new NpgsqlConnection(masterConnectionStringBuilder.ConnectionString))
            {
                connection.Open();

                var sqlCommandText = string.Format
                (
                    @"SELECT case WHEN oid IS NOT NULL THEN 1 ELSE 0 end FROM pg_database WHERE datname = '{0}' limit 1;",
                    databaseName
                );


                // check to see if the database already exists..
                using (var command = new NpgsqlCommand(sqlCommandText, connection)
                {
                    CommandType = CommandType.Text
                })
                {
                    var results = (int?)command.ExecuteScalar();

                    // if the database does not exists, we're done here...
                    if (!results.HasValue || results.Value != 1)
                    {
                        return;
                    }
                }

                sqlCommandText = string.Format
                (
                    "UPDATE pg_database SET datallowconn = \'false\' WHERE datname = \'{0}\';" +
                    "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = \'{0}\';" +
                    "drop database \"{0}\";",
                    databaseName
                );

                // Create the database...
                using (var command = new NpgsqlCommand(sqlCommandText, connection)
                {
                    CommandType = CommandType.Text
                })
                {
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}