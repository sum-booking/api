﻿CREATE TABLE Administrators
(
	AdministratorId VARCHAR(128) NOT NULL,
	Email VARCHAR(500) NOT NULL UNIQUE,
	PhoneNumber VARCHAR(500) NOT NULL,
	FirstName VARCHAR(500) NOT NULL,
	LastName VARCHAR(500) NOT NULL,
	PictureUrl VARCHAR(2048) NULL,
	CreatedBy VARCHAR(500) NOT NULL,
	CreatedOn TIMESTAMP WITH TIME ZONE NOT NULL,
    UpdatedBy VARCHAR(500) NOT NULL,
    UpdatedOn TIMESTAMP WITH TIME ZONE NOT NULL,

	PRIMARY KEY (AdministratorId)
);