﻿ALTER TABLE Consortiums
	ADD COLUMN AdministratorId VARCHAR(128) NULL;
ALTER TABLE Consortiums
	ADD FOREIGN KEY (AdministratorId) REFERENCES Administrators(AdministratorId);