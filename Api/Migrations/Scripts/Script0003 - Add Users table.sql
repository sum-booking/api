﻿CREATE TABLE Users
(
	UserId VARCHAR(128) NOT NULL,
	ConsortiumCode VARCHAR(8) NOT NULL,
	FunctionalUnitCode VARCHAR(8) NOT NULL,
	Email VARCHAR(500) NOT NULL UNIQUE,
	PhoneNumber VARCHAR(500) NOT NULL,
	FirstName VARCHAR(500) NOT NULL,
	LastName VARCHAR(500) NOT NULL,
	PictureUrl VARCHAR(2048) NULL,
	PhoneNotifications BOOLEAN NOT NULL,
	EmailNotifications BOOLEAN NOT NULL,
	CreatedBy VARCHAR(500) NOT NULL,
	CreatedOn TIMESTAMP WITH TIME ZONE NOT NULL,
    UpdatedBy VARCHAR(500) NOT NULL,
    UpdatedOn TIMESTAMP WITH TIME ZONE NOT NULL,

	PRIMARY KEY (UserId, FunctionalUnitCode, ConsortiumCode),
	FOREIGN KEY (ConsortiumCode, FunctionalUnitCode) REFERENCES FunctionalUnits(ConsortiumCode, Code)
);