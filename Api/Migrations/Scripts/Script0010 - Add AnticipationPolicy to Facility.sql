﻿ALTER TABLE Facilities
	ADD COLUMN AnticipationPolicy VARCHAR(128) NULL CHECK(AnticipationPolicy IN ('SevenDays'));

UPDATE Facilities SET AnticipationPolicy = 'SevenDays';

ALTER TABLE Facilities
	ALTER COLUMN AnticipationPolicy SET NOT NULL;