﻿ALTER TABLE Facilities
	ADD COLUMN CancellationPolicy VARCHAR(128) NULL CHECK(CancellationPolicy IN ('SeventyTwoHours', 'FortyEightHours'));

UPDATE Facilities SET CancellationPolicy = 'SeventyTwoHours';

ALTER TABLE Facilities
	ALTER COLUMN CancellationPolicy SET NOT NULL;