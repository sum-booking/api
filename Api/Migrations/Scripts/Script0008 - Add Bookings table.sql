﻿CREATE TABLE Bookings (
	Id INTEGER NOT NULL DEFAULT 0,
	ConsortiumCode VARCHAR(8) NOT NULL REFERENCES Consortiums(Code),
	FunctionalUnitCode VARCHAR(8) NOT NULL,
	FacilityId INTEGER NOT NULL,	
	BookDate TIMESTAMP WITH TIME ZONE NOT NULL,
	Strategy VARCHAR(64) NOT NULL CHECK(Strategy IN ('NightOnly', 'MidDayAndNight')),
	IsMidDay BOOLEAN NULL,
	IsNight BOOLEAN NULL,
	CreatedBy VARCHAR(500) NOT NULL,
	CreatedOn TIMESTAMP WITH TIME ZONE NOT NULL,
	UpdatedBy VARCHAR(500) NOT NULL,
	UpdatedOn TIMESTAMP WITH TIME ZONE NOT NULL,
	Deleted BOOLEAN NOT NULL DEFAULT(FALSE),

	PRIMARY KEY (Id, ConsortiumCode),
	FOREIGN KEY (ConsortiumCode, FunctionalUnitCode) REFERENCES FunctionalUnits(ConsortiumCode, Code),
	FOREIGN KEY (ConsortiumCode, FacilityId) REFERENCES Facilities(ConsortiumCode, Id)
);

CREATE FUNCTION Bookings_id_auto()
    RETURNS trigger AS $$
DECLARE
    _rel_id constant int := 'Bookings'::regclass::int;
    _grp_id int;
BEGIN
	_grp_id = hashtext(NEW.ConsortiumCode);

	-- Obtain an advisory lock on this table/group.
    PERFORM pg_advisory_xact_lock(_rel_id, _grp_id);

    SELECT  COALESCE(MAX(id) + 1, 1)
    INTO    NEW.id
    FROM    Bookings
    WHERE   ConsortiumCode = NEW.ConsortiumCode;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql STRICT;

CREATE TRIGGER Bookings_id_auto
    BEFORE INSERT ON Bookings
    FOR EACH ROW WHEN (NEW.id = 0)
    EXECUTE PROCEDURE Bookings_id_auto();
