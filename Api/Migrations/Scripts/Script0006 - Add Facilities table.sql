﻿CREATE TABLE Facilities (
	Id INTEGER NOT NULL DEFAULT 0,
	ConsortiumCode VARCHAR(8) NOT NULL REFERENCES Consortiums(Code),
	Name VARCHAR(500) NOT NULL,
	BookingSchedule VARCHAR(64) NOT NULL CHECK(BookingSchedule IN ('NightOnly', 'MidDayAndNight')),
	BookingLimit VARCHAR(64) NOT NULL CHECK(BookingLimit IN ('TwiceAMonth')),
	CreatedBy VARCHAR(500) NOT NULL,
	CreatedOn TIMESTAMP WITH TIME ZONE NOT NULL,
	UpdatedBy VARCHAR(500) NOT NULL,
	UpdatedOn TIMESTAMP WITH TIME ZONE NOT NULL,
	PRIMARY KEY (Id, ConsortiumCode)
);

CREATE FUNCTION Facilities_id_auto()
    RETURNS trigger AS $$
DECLARE
    _rel_id constant int := 'Facilities'::regclass::int;
    _grp_id int;
BEGIN
	_grp_id = hashtext(NEW.ConsortiumCode);

	-- Obtain an advisory lock on this table/group.
    PERFORM pg_advisory_xact_lock(_rel_id, _grp_id);

    SELECT  COALESCE(MAX(id) + 1, 1)
    INTO    NEW.id
    FROM    Facilities
    WHERE   ConsortiumCode = NEW.ConsortiumCode;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql STRICT;

CREATE TRIGGER Facilities_id_auto
    BEFORE INSERT ON Facilities
    FOR EACH ROW WHEN (NEW.id = 0)
    EXECUTE PROCEDURE Facilities_id_auto();
