﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;

namespace Api.Repositories
{
    /// <summary>
    /// Functional unit repository
    /// </summary>
    public interface IFunctionalUnitRepository
    {
        /// <summary>
        /// Check conflicts
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<bool> ConflictsAsync(FunctionalUnitCreateViewModel model, string consortiumCode);

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<IEnumerable<FunctionalUnit>> GetAllAsync(string consortiumCode, int? offset = 0, int? limit = 0);

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <returns></returns>
        Task<FunctionalUnit> GetByIdAsync(string consortiumCode, string functionalUnitCode);

        /// <summary>
        /// Save functional unit
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<FunctionalUnit> SaveAsync(string consortiumCode, FunctionalUnitCreateViewModel model);

        /// <summary>
        /// Check if exists
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <returns></returns>
        Task<bool> ExistsAsync(string consortiumCode, string functionalUnitCode);
    }
}