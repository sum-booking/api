﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Repositories
{
    /// <summary>
    /// User repository
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Check conflicts
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> CheckConflictsAsync(UserCreateViewModel model);

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<IEnumerable<UserViewModel>> GetAllAsync(string consortiumCode, string functionalUnitCode, int? offset = 0, int? limit = 1000);

        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserViewModel> GetUserByIdAsync(string consortiumCode, string functionalUnitCode, string userId);

        /// <summary>
        /// Save user
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <param name="userId"></param>
        /// <param name="pictureUrl"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task SaveAsync(string consortiumCode, string functionalUnitCode, string userId, string pictureUrl, UserCreateViewModel model);
    }
}