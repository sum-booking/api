﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Repositories
{
    /// <inheritdoc />
    public class FunctionalUnitRepository : IFunctionalUnitRepository
    {
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public FunctionalUnitRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<FunctionalUnit>> GetAllAsync(string consortiumCode, int? offset = 0,
            int? limit = 0)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QueryAsync<FunctionalUnit>(
                    @"SELECT * FROM functionalUnits WHERE ConsortiumCode = @consortiumCode LIMIT @limit OFFSET @offset", new
                    {
                        consortiumCode,
                        limit,
                        offset
                    });
            }
        }

        /// <inheritdoc />
        public async Task<FunctionalUnit> GetByIdAsync(string consortiumCode, string functionalUnitCode)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<FunctionalUnit>(
                    @"SELECT * FROM functionalUnits WHERE Code = @functionalUnitCode AND ConsortiumCode = @consortiumCode", new
                    {
                        consortiumCode,
                        functionalUnitCode
                    });
            }
        }

        /// <inheritdoc />
        public async Task<bool> ConflictsAsync(FunctionalUnitCreateViewModel model,
            string consortiumCode)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<bool>("SELECT 1 FROM functionalUnits WHERE " +
                                                                        "Code ILIKE @code AND " +
                                                                        "ConsortiumCode = @consortiumCode",
                    new { code = model.Code, consortiumCode });
            }
        }

        /// <inheritdoc />
        public async Task<FunctionalUnit> SaveAsync(string consortiumCode, FunctionalUnitCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleAsync<FunctionalUnit>(
                    "INSERT INTO functionalUnits(Code, ConsortiumCode, Designation, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn) " +
                    "VALUES(@code, @consortiumCode, @designation, @createdBy, @createdOn, @createdBy, @createdOn) " +
                    "RETURNING *", new
                    {
                        code = model.Code,
                        consortiumCode,
                        designation = model.Designation,
                        createdBy = model.CreatedBy,
                        createdOn = model.CreatedOn
                    });
            }
        }

        /// <inheritdoc />
        public async Task<bool> ExistsAsync(string consortiumCode, string functionalUnitCode)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<bool>(
                    "SELECT 1 FROM functionalUnits WHERE Code = @functionalUnitCode " +
                    " AND ConsortiumCode = @consortiumCode",
                    new { consortiumCode, functionalUnitCode });
            }
        }
    }
}