﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Repositories
{
    /// <inheritdoc />
    public class ConsortiumRepository : IConsortiumRepository
    {
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public ConsortiumRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<Consortium> GetAsync(string consortiumCode)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<Consortium>(
                    @"SELECT * FROM consortiums WHERE Code = @consortiumCode and Deleted = false", new
                    {
                        consortiumCode
                    });
            }
        }

        /// <summary>
        /// Validate existance
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        public async Task<bool> ExistsAsync(string consortiumCode)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<bool>(
                    "SELECT 1 FROM consortiums WHERE Code ILIKE @consortiumCode",
                    new {consortiumCode});
            }
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Consortium>> GetAllAsync(IEnumerable<string> consortiumCodes, int? offset = 0,
            int? limit = 1000)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                var query = new StringBuilder(@"SELECT * FROM consortiums");
                if (consortiumCodes.Any())
                {
                    query.Append(" WHERE code = ANY(@consortiumCodes)");
                }

                query.Append(" LIMIT @limit OFFSET @offset");
                return await connection.QueryAsync<Consortium>(
                    query.ToString(), new
                    {
                        limit,
                        offset,
                        consortiumCodes
                    });
            }
        }

        /// <inheritdoc />
        public async Task<Consortium> SaveAsync(ConsortiumCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleAsync<Consortium>(
                    "INSERT INTO consortiums(Code, Name, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn) " +
                    "VALUES(@code, @name, @createdBy, @createdOn, @createdBy, @createdOn) " +
                    "RETURNING *", new
                    {
                        code = model.Code,
                        name = model.Name,
                        createdBy = model.CreatedBy,
                        createdOn = model.CreatedOn
                    });
            }
        }

        /// <inheritdoc />
        public async Task UpdateAdministratorIdAsync(string administratorId, string consortiumCode)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                const string sql =
                    "UPDATE consortiums SET AdministratorId = @administratorId WHERE code = @consortiumCode";
                await connection.ExecuteAsync(sql, new
                {
                    administratorId,
                    consortiumCode
                });
            }
        }
    }
}