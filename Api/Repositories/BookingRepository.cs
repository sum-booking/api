﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Repositories
{
    /// <inheritdoc />
    public class BookingRepository : IBookingRepository
    {
        /// <summary>
        /// Configuration
        /// </summary>
        protected readonly IConfiguration Configuration;

        /// <inheritdoc />
        public BookingRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<int> SaveAsync(string consortiumCode, BookingCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(Configuration.GetConnectionString("ApiDatabase")))
            {
                const string sql = "INSERT INTO bookings (ConsortiumCode, FunctionalUnitCode, FacilityId, BookDate, " +
                                   "IsMidDay, IsNight, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn) " +
                                   "VALUES (@consortiumCode, @functionalUnitCode, @facilityId, @bookDate, " +
                                   "@isMidDay, @isNight, @createdBy, @createdOn, @createdBy, @createdOn) RETURNING Id";
                return await connection.QuerySingleAsync<int>(sql, new
                {
                    consortiumCode,
                    functionalUnitCode = model.FunctionalUnitCode,
                    facilityId = model.FacilityId,
                    bookDate = model.BookDate,
                    isMidDay = model.IsMidDay,
                    isNight = model.IsNight,
                    createdBy = model.CreatedBy,
                    createdOn = model.CreatedOn
                });
            }

        }

        /// <inheritdoc />
        public async Task DeleteAsync(string consortiumCode, int id, string updatedBy, DateTimeOffset updatedOn)
        {
            using (var connection = new NpgsqlConnection(Configuration.GetConnectionString("ApiDatabase")))
            {
                const string sql =
                    @"UPDATE bookings SET Deleted = true, UpdatedBy = @updatedBy, UpdatedOn = @updatedOn WHERE ConsortiumCode = @consortiumCode AND Id = @id";

                await connection.ExecuteAsync(sql, new
                {
                    consortiumCode,
                    id,
                    updatedBy,
                    updatedOn
                });
            }
        }

        /// <inheritdoc />
        public async Task<BookingViewModel> GetAsync(string consortiumCode, int id)
        {
            using (var connection = new NpgsqlConnection(Configuration.GetConnectionString("ApiDatabase")))
            {
                const string sql = @"SELECT * FROM bookings b " +
                                   "INNER JOIN facilities f ON f.Id = b.FacilityId AND f.ConsortiumCode = @consortiumCode " +
                                   "INNER JOIN functionalUnits fu on fu.Code = b.FunctionalUnitCode AND fu.ConsortiumCode = @consortiumCode " +
                                   "WHERE b.ConsortiumCode = @consortiumCode AND b.Id = @id AND b.Deleted = false ";
                var result = await connection.QueryAsync<Booking, Facility, FunctionalUnit, BookingViewModel>(sql,
                    TransformToModel,
                    new
                    {
                        consortiumCode,
                        id
                    },
                    splitOn: "Id,Code");

                return result.FirstOrDefault();
            }
        }

        /// <inheritdoc />
        public async Task<IEnumerable<BookingViewModel>> GetAllAsync(string consortiumCode, DateTimeOffset from,
            DateTimeOffset to,
            int? offset = null, int? limit = null, int? facilityId = null, string userId = null)
        {
            using (var connection = new NpgsqlConnection(Configuration.GetConnectionString("ApiDatabase")))
            {
                var sql = @"SELECT b.*, f.*, fu.* FROM bookings b " +
                          "INNER JOIN facilities f ON f.Id = b.FacilityId AND f.ConsortiumCode = b.ConsortiumCode " +
                          "INNER JOIN functionalUnits fu on fu.Code = b.FunctionalUnitCode AND fu.ConsortiumCode = b.ConsortiumCode ";

                if (!string.IsNullOrWhiteSpace(userId))
                {
                    sql += "INNER JOIN users u ON u.FunctionalUnitCode = b.FunctionalUnitCode AND u.ConsortiumCode = b.ConsortiumCode ";
                }
                sql += "WHERE b.ConsortiumCode = @consortiumCode AND b.Deleted = false " +
                       "AND date_trunc('day', b.BookDate) BETWEEN date_trunc('day', @from) AND date_trunc('day', @to) ";

                if (facilityId != null)
                {
                    sql += "AND b.facilityId = @facilityId ";
                }

                if (!string.IsNullOrWhiteSpace(userId))
                {
                    sql += "AND u.UserId = @userId ";
                }

                sql += "ORDER BY b.BookDate ";

                if (offset != null)
                {
                    sql += "OFFSET @offset ";
                }

                if (limit != null)
                {
                    sql += "LIMIT @limit ";
                }

                return await connection.QueryAsync<Booking, Facility, FunctionalUnit, BookingViewModel>(sql,
                    TransformToModel,
                    new
                    {
                        consortiumCode,
                        from,
                        to,
                        offset,
                        limit,
                        facilityId,
                        userId,
                    },
                    splitOn: "Id,Code");
            }
        }

        private static BookingViewModel TransformToModel(Booking booking, Facility facility, FunctionalUnit functionalUnit)
        {
            return new BookingViewModel
            {
                CreatedOn = booking.CreatedOn,
                CreatedBy = booking.CreatedBy,
                Id = booking.Id,
                BookDate = booking.BookDate,
                FacilityId = booking.FacilityId,
                FunctionalUnitCode = booking.FunctionalUnitCode,
                ConsortiumCode = booking.ConsortiumCode,
                Deleted = booking.Deleted,
                FacilityName = facility.Name,
                FunctionalUnitDesignation = functionalUnit.Designation,
                IsMidDay = booking.IsMidDay,
                IsNight = booking.IsNight,
                Strategy = facility.BookingSchedule,
                UpdatedBy = booking.UpdatedBy,
                UpdatedOn = booking.UpdatedOn,
            };
        }
    }
}