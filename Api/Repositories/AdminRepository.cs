﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Repositories
{
    /// <inheritdoc />
    public class AdminRepository : IAdminRepository
    {
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public AdminRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<AdministratorViewModel>> GetAllAsync(int? offset = 0, int? limit = 1000)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                const string sql = "SELECT * FROM administrators a " +
                                   " INNER JOIN consortiums c ON c.AdministratorId = a.AdministratorId " +
                                   " LIMIT @limit OFFSET @offset";
                var admins = await connection.QueryAsync<AdministratorViewModel, Consortium, AdministratorViewModel>(sql,
                    (admin, consortium) =>
                    {
                        if (admin.Consortiums == null) admin.Consortiums = new List<Consortium>();
                        admin.Consortiums.Add(consortium);
                        return admin;
                    },
                    new
                    {
                        limit,
                        offset
                    },
                    splitOn: "Code");

                admins = admins.GroupBy(o => o.AdministratorId).Select(group =>
                {
                    var combinedAdmin = group.First();
                    combinedAdmin.Consortiums = group.Select(admin => admin.Consortiums?.SingleOrDefault()).ToList();
                    return combinedAdmin;
                }).ToList();

                return admins;
            }
        }

        /// <inheritdoc />
        public async Task<AdministratorViewModel> GetAdministratorByIdAsync(string administratorId)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                const string sql = "SELECT * FROM administrators a " +
                                   " INNER JOIN consortiums c ON c.AdministratorId = a.AdministratorId " +
                                   " WHERE a.AdministratorId = @administratorId ";
                var admins = await connection.QueryAsync<AdministratorViewModel, Consortium, AdministratorViewModel>(sql,
                    (model, consortium) =>
                    {
                        if (model.Consortiums == null) model.Consortiums = new List<Consortium>();
                        model.Consortiums.Add(consortium);
                        return model;
                    },
                    new
                    {
                        administratorId
                    },
                    splitOn: "Code");

                return admins.GroupBy(o => o.AdministratorId).Select(group =>
                {
                    var combinedAdmin = group.First();
                    combinedAdmin.Consortiums = group.Select(admin => admin.Consortiums?.SingleOrDefault()).ToList();
                    return combinedAdmin;
                }).SingleOrDefault();
            }
        }

        /// <inheritdoc />
        public async Task SaveAsync(AdministratorCreateViewModel model, string userId, string picture)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                await connection.ExecuteAsync(
                    "INSERT INTO Administrators(AdministratorId, PictureUrl, Email, PhoneNumber, FirstName, " +
                    "LastName, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn) VALUES(@administratorId, @pictureUrl," +
                    " @email, @phoneNumber, @firstName, @lastName, @createdBy, @createdOn, @createdBy, " +
                    "@createdOn) ", new
                    {
                        administratorId = userId,
                        pictureUrl = picture,
                        email = model.Email,
                        phoneNumber = model.PhoneNumber,
                        firstName = model.FirstName,
                        lastName = model.LastName,
                        createdBy = model.CreatedBy,
                        createdOn = model.CreatedOn,
                    });
            }
        }

        /// <inheritdoc />
        public async Task<bool> ConflictsAsync(AdministratorCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<bool>(
                    "SELECT 1 FROM administrators WHERE Email ILIKE @email",
                    new {email = model.Email});
            }
        }
    }
}