﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;

namespace Api.Repositories
{
    /// <summary>
    /// Consortium repository
    /// </summary>
    public interface IConsortiumRepository
    {
        /// <summary>
        /// Get consortium
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<Consortium> GetAsync(string consortiumCode);

        /// <summary>
        /// Check if consortium exits
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<bool> ExistsAsync(string consortiumCode);

        /// <summary>
        /// Get all consortiums
        /// </summary>
        /// <param name="consortiumCodes"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<IEnumerable<Consortium>> GetAllAsync(IEnumerable<string> consortiumCodes, int? offset = 0,
            int? limit = 1000);

        /// <summary>
        /// Save consortium
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Consortium> SaveAsync(ConsortiumCreateViewModel model);

        /// <summary>
        /// Update administrator Id
        /// </summary>
        /// <param name="administratorId"></param>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task UpdateAdministratorIdAsync(string administratorId, string consortiumCode);
    }
}