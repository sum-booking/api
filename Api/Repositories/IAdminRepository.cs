﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Repositories
{
    /// <summary>
    /// Admin repository
    /// </summary>
    public interface IAdminRepository
    {
        /// <summary>
        /// Get All
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<IEnumerable<AdministratorViewModel>> GetAllAsync(int? offset = 0, int? limit = 1000);

        /// <summary>
        /// Get Administrator by Id
        /// </summary>
        /// <param name="administratorId"></param>
        /// <returns></returns>
        Task<AdministratorViewModel> GetAdministratorByIdAsync(string administratorId);

        /// <summary>
        /// Save administrator
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <param name="picture"></param>
        /// <returns></returns>
        Task SaveAsync(AdministratorCreateViewModel model, string userId, string picture);

        /// <summary>
        /// Check conflicts
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> ConflictsAsync(AdministratorCreateViewModel model);
    }
}