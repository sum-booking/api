﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Repositories
{
    /// <inheritdoc />
    public class FacilityRepository : IFacilityRepository
    {
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public FacilityRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<Facility> GetAsync(string consortiumCode, int id)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<Facility>(
                    @"SELECT * FROM facilities WHERE Id = @id AND ConsortiumCode = @consortiumCode", new
                    {
                        consortiumCode,
                        id
                    });
            }
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Facility>> GetAllAsync(string consortiumCode, int? offset = 0, int? limit = 0)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QueryAsync<Facility>(
                     @"SELECT * FROM facilities WHERE ConsortiumCode = @consortiumCode LIMIT @limit OFFSET @offset", new
                     {
                         consortiumCode,
                         limit,
                         offset
                     });
            }
        }

        /// <inheritdoc />
        public async Task<bool> ConflictsAsync(string consortiumCode, FacilityCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<bool>("SELECT 1 FROM facilities WHERE " +
                                                                        "Name ILIKE @name AND " +
                                                                        "ConsortiumCode = @consortiumCode",
                    new { name = model.Name, consortiumCode });
            }
        }

        /// <inheritdoc />
        public async Task<Facility> SaveAsync(string consortiumCode, FacilityCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleAsync<Facility>(
                    "INSERT INTO facilities(ConsortiumCode, Name," +
                    "BookingSchedule, BookingLimit, CancellationPolicy, AnticipationPolicy, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn) " +
                    "VALUES(@consortiumCode, @name, @bookingSchedule, @bookingLimit, @cancellationPolicy, @anticipationPolicy, " +
                    "@createdBy, @createdOn, @createdBy, @createdOn) " +
                    "RETURNING *", new
                    {
                        consortiumCode,
                        name = model.Name,
                        bookingSchedule = model.BookingSchedule.ToString(),
                        bookingLimit = model.BookingLimit.ToString(),
                        cancellationPolicy = model.CancellationPolicy.ToString(),
                        anticipationPolicy = model.AnticipationPolicy.ToString(),
                        createdBy = model.CreatedBy,
                        createdOn = model.CreatedOn
                    });
            }
        }

        /// <summary>
        /// Validate existance
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="facilityId"></param>
        /// <returns></returns>
        public async Task<bool> ExistsAsync(string consortiumCode, int facilityId)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<bool>(
                    "SELECT 1 FROM facilities WHERE Id = @facilityId " +
                    " AND ConsortiumCode = @consortiumCode",
                    new { consortiumCode, facilityId });
            }
        }
    }
}