﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;

namespace Api.Repositories
{
    /// <summary>
    /// Facility repository
    /// </summary>
    public interface IFacilityRepository
    {
        /// <summary>
        /// Get facility
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Facility> GetAsync(string consortiumCode, int id);

        /// <summary>
        /// Get all facilities
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<IEnumerable<Facility>> GetAllAsync(string consortiumCode, int? offset = 0, int? limit = 0);

        /// <summary>
        /// Check if there a conflict with another facility
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> ConflictsAsync(string consortiumCode, FacilityCreateViewModel model);

        /// <summary>
        /// Save facility
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Facility> SaveAsync(string consortiumCode, FacilityCreateViewModel model);

        /// <summary>
        /// Check if exists
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="facilityId"></param>
        /// <returns></returns>
        Task<bool> ExistsAsync(string consortiumCode, int facilityId);
    }
}