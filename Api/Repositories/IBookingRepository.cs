﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Repositories
{
    /// <summary>
    /// Booking repository
    /// </summary>
    public interface IBookingRepository
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BookingViewModel> GetAsync(string consortiumCode, int id);

        /// <summary>
        /// Get all
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="facilityId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<IEnumerable<BookingViewModel>> GetAllAsync(string consortiumCode, DateTimeOffset from, DateTimeOffset to,
            int? offset = null, int? limit = null, int? facilityId = null, string userId = null);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        /// <returns></returns>
        Task<int> SaveAsync(string consortiumCode, BookingCreateViewModel model);

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="id"></param>
        /// <param name="updatedBy"></param>
        /// <param name="updatedOn"></param>
        /// <returns></returns>
        Task DeleteAsync(string consortiumCode, int id, string updatedBy, DateTimeOffset updatedOn);
    }
}