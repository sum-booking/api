﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Repositories
{
    /// <inheritdoc />
    public class UserRepository : IUserRepository
    {
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public UserRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<UserViewModel>> GetAllAsync(string consortiumCode, string functionalUnitCode,
            int? offset = 0, int? limit = 1000)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                var sql = @"SELECT u.*,c.Name as Consortium,fu.Designation as FunctionalUnit FROM users u " +
                          " INNER JOIN consortiums c ON c.Code = u.ConsortiumCode " +
                          " INNER JOIN functionalUnits fu on fu.Code = u.FunctionalUnitCode AND fu.ConsortiumCode = u.ConsortiumCode " +
                          " WHERE u.ConsortiumCode = @consortiumCode ";

                if (functionalUnitCode != null)
                {
                    sql += " AND u.FunctionalUnitCode = @functionalUnitCode ";
                }

                return await connection.QueryAsync<UserViewModel>(sql + " LIMIT @limit OFFSET @offset", new
                {
                    functionalUnitCode,
                    consortiumCode,
                    limit,
                    offset
                });
            }
        }

        /// <inheritdoc />
        public async Task<UserViewModel> GetUserByIdAsync(string consortiumCode, string functionalUnitCode, string userId)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                var user = await connection.QuerySingleOrDefaultAsync<UserViewModel>(
                    @"SELECT u.*,c.Name as Consortium,fu.Designation as FunctionalUnit FROM users u " +
                    " INNER JOIN consortiums c ON c.Code = u.ConsortiumCode " +
                    " INNER JOIN functionalUnits fu on fu.Code = u.FunctionalUnitCode AND fu.ConsortiumCode = u.ConsortiumCode " +
                    " WHERE u.ConsortiumCode = @consortiumCode AND u.FunctionalUnitCode = @functionalUnitCode " +
                    " AND u.UserId = @userId", new
                    {
                        functionalUnitCode,
                        consortiumCode,
                        userId,
                    });
                return user;
            }
        }

        /// <inheritdoc />
        public async Task<bool> CheckConflictsAsync(UserCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                return await connection.QuerySingleOrDefaultAsync<bool>("SELECT 1 FROM users WHERE Email ILIKE @email",
                    new {email = model.Email});
            }
        }

        /// <inheritdoc />
        public async Task SaveAsync(string consortiumCode, string functionalUnitCode, string userId, string pictureUrl,
            UserCreateViewModel model)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                await connection.ExecuteAsync(
                    "INSERT INTO Users(UserId, PictureUrl, ConsortiumCode, FunctionalUnitCode, Email, PhoneNumber, FirstName, " +
                    "LastName, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, PhoneNotifications, EmailNotifications) VALUES(@userId, @pictureUrl, @consortiumCode," +
                    " @functionalUnitCode, @email, @phoneNumber, @firstName, @lastName, @createdBy, @createdOn, @createdBy, " +
                    "@createdOn, @phoneNotifications, @emailNotifications) ", new
                    {
                        userId,
                        pictureUrl,
                        consortiumCode,
                        functionalUnitCode,
                        email = model.Email,
                        phoneNumber = model.PhoneNumber,
                        firstName = model.FirstName,
                        lastName = model.LastName,
                        createdBy = model.CreatedBy,
                        createdOn = model.CreatedOn,
                        phoneNotifications = model.PhoneNotifications,
                        emailNotifications = model.EmailNotifications,
                    });
            }
        }
    }
}