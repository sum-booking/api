﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Services.Limits
{
    /// <summary>
    /// Booking limit strategy
    /// </summary>
    public interface IBookingLimitStrategy
    {
        /// <summary>
        /// Test strategy
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="booking"></param>
        /// <returns>error, message if has tested limit, null otherwise</returns>
        Task<IDictionary<string, string>> TestLimitAsync(string consortiumCode, BookingCreateViewModel booking);
    }
}