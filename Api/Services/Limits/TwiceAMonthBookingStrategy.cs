﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Services.Limits
{
    /// <inheritdoc />
    public class TwiceAMonthBookingStrategy : IBookingLimitStrategy
    {
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Constructor
        /// </summary>
        public TwiceAMonthBookingStrategy(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestLimitAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            using (var connection = new NpgsqlConnection(_configuration.GetConnectionString("ApiDatabase")))
            {
                var sql = @"SELECT COUNT(*) FROM bookings WHERE consortiumCode = @consortiumCode " +
                           " AND functionalUnitCode = @functionalUnitCode AND facilityId = @facilityId" +
                           " AND date_trunc('month', BookDate) = date_trunc('month', @bookDate) AND Deleted = false";
                var count = await connection.ExecuteScalarAsync<int>(sql, new
                {
                    consortiumCode,
                    functionalUnitCode = booking.FunctionalUnitCode,
                    facilityId = booking.FacilityId,
                    bookDate = booking.BookDate
                });

                return count == 2
                    ? await Task.FromResult(new Dictionary<string, string>
                    {
                        { "TWICE_A_MONTH","The functional unit alreayd book twice this month"}
                    })
                    : null;
            }
        }
    }
}