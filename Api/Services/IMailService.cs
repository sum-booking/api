﻿using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Services
{
    /// <summary>
    /// Mail Service
    /// </summary>
    public interface IMailService
    {
        /// <summary>
        /// Send welcome email
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        Task SendWelcomeEmailAsync(UserViewModel model, string consortiumCode);
    }
}