﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Services.Schedules
{
    /// <inheritdoc />
    public class SchedulingStrategyFactory : ISchedulingStrategyFactory
    {
        private readonly IFacilityRepository _facilityRepository;
        private readonly Func<BookingScheduleStrategy, ISchedulingStrategy> _factory;

        /// <inheritdoc />
        public SchedulingStrategyFactory(IFacilityRepository facilityRepository, Func<BookingScheduleStrategy, ISchedulingStrategy> factory)
        {
            _facilityRepository = facilityRepository;
            _factory = factory;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestScheduleAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var strategy = await GetStrategyAsync(consortiumCode, booking);        
            return await strategy.TestScheduleAsync(consortiumCode, booking);
        }

        private async Task<ISchedulingStrategy> GetStrategyAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var facility = await _facilityRepository.GetAsync(consortiumCode, booking.FacilityId);
            var strategy = _factory(facility.BookingSchedule);
            return strategy;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateSchedulePropertiesAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var strategy = await GetStrategyAsync(consortiumCode, booking);
            return await strategy.ValidateSchedulePropertiesAsync(consortiumCode, booking);
        }
    }
}