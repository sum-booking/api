﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Services.Schedules
{
    /// <summary>
    /// Scheduling strategy
    /// </summary>
    public interface ISchedulingStrategy
    {
        /// <summary>
        /// Test schedule strategy
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="booking"></param>
        /// <returns>error, message if has tested schedule, null otherwise</returns>
        Task<IDictionary<string, string>> TestScheduleAsync(string consortiumCode, BookingCreateViewModel booking);

        /// <summary>
        /// Test schedule properties
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="booking"></param>
        /// <returns>return dictionary of errors or null otherwise</returns>
        Task<IDictionary<string, string>> ValidateSchedulePropertiesAsync(string consortiumCode, BookingCreateViewModel booking);
    }
}