﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Plumbing;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Services.Schedules
{
    /// <inheritdoc />
    [ScheduleStrategyKey(BookingScheduleStrategy.MidDayAndNight)]
    public class MidDayAndNightScheduleStrategy : ISchedulingStrategy
    {
        private readonly IBookingRepository _repository;

        /// <inheritdoc />
        public MidDayAndNightScheduleStrategy(IBookingRepository repository)
        {
            _repository = repository;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestScheduleAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var bookings = await _repository.GetAllAsync(consortiumCode, booking.BookDate, booking.BookDate, facilityId: booking.FacilityId);

            if (bookings.Count() == 1 && (bookings.All(x => x.IsMidDay == booking.IsMidDay) ||
                                          bookings.All(x => x.IsNight == booking.IsNight)))
            {
                return await Task.FromResult(new Dictionary<string, string>
                {
                    {"ALREADY_BOOKED", "The facility is already booked for the date"}
                });
            }

            if (bookings.Count() == 2 && bookings.Any(x => x.IsMidDay.GetValueOrDefault(false)) &&
                bookings.Any(x => x.IsNight.GetValueOrDefault(false)))
            {
                return await Task.FromResult(new Dictionary<string, string>
                {
                    {"ALREADY_BOOKED", "The facility is already booked for the date"}
                });
            }

            return null;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateSchedulePropertiesAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var errors = new Dictionary<string, string>();
            if (!booking.IsMidDay.HasValue)
            {
                errors.Add("IsMidDay", "Is required");
            }

            if (!booking.IsNight.HasValue)
            {
                errors.Add("IsNight", "Is required");
            }

            if (booking.IsNight.HasValue && booking.IsMidDay.HasValue && booking.IsNight.Value == booking.IsMidDay.Value)
            {
                errors.Add("BOOK_WRONG", "Value for time of day should be different");
            }

            return await Task.FromResult(errors);
        }
    }
}