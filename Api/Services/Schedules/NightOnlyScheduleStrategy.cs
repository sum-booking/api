﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Plumbing;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Services.Schedules
{
    /// <inheritdoc />
    [ScheduleStrategyKey(BookingScheduleStrategy.NightOnly)]
    public class NightOnlyScheduleStrategy : ISchedulingStrategy
    {
        private readonly IBookingRepository _repository;

        /// <inheritdoc />
        public NightOnlyScheduleStrategy(IBookingRepository repository)
        {
            _repository = repository;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestScheduleAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var bookings = await _repository.GetAllAsync(consortiumCode, booking.BookDate, booking.BookDate, facilityId: booking.FacilityId);

            return bookings.Any()
                ? await Task.FromResult(new Dictionary<string, string>
                {
                    { "ALREADY_BOOKED", "The facility is already booked for the date" }
                })
                : null;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> ValidateSchedulePropertiesAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            return await Task.FromResult(new Dictionary<string, string>());
        }
    }
}