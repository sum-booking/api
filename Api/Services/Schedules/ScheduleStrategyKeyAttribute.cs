﻿using System;
using Api.Models.Enums;

namespace Api.Services.Schedules
{
    /// <inheritdoc />
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ScheduleStrategyKeyAttribute : Attribute
    {
        /// <summary>
        /// Value
        /// </summary>
        public BookingScheduleStrategy Value { get; }

        /// <inheritdoc />
        public ScheduleStrategyKeyAttribute(BookingScheduleStrategy value)
        {
            Value = value;
        }
    }
}