﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Services.AnticipationPolicies
{
    /// <summary>
    /// Anticipation policy
    /// </summary>
    public interface IBookingAnticipationPolicy
    {
        /// <summary>
        /// Test anticipation policy
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="booking"></param>
        /// <returns>error, message if has tested anticipation, null otherwise</returns>
        Task<IDictionary<string, string>> TestAnticipationPolicyAsync(string consortiumCode, BookingCreateViewModel booking);
    }
}