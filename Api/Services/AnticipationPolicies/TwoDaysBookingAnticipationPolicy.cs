﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Plumbing;
using Api.ViewModels;

namespace Api.Services.AnticipationPolicies
{
    /// <inheritdoc />
    [AnticipationPolicyKey(BookingAnticipationPolicy.TwoDays)]
    public class TwoDaysBookingAnticipationPolicy : IBookingAnticipationPolicy
    {
        private readonly TimeSpan _twoDays = TimeSpan.FromDays(2);

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestAnticipationPolicyAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var differenceUntilToday = booking.BookDate.Trim(TimeSpan.TicksPerDay) - DateTimeOffset.Now.Trim(TimeSpan.TicksPerDay);
            if (differenceUntilToday < _twoDays)
            {
                return await Task.FromResult(new Dictionary<string, string>
                {
                    {"2DAYS_ANTICIPATION", "You can only book the facility with 2 days in advance"}
                });
            }

            return null;
        }
    }
}