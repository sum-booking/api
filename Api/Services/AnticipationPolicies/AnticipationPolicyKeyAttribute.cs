﻿using System;
using Api.Models.Enums;

namespace Api.Services.AnticipationPolicies
{
    /// <inheritdoc />
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class AnticipationPolicyKeyAttribute : Attribute
    {
        /// <summary>
        /// Value
        /// </summary>
        public BookingAnticipationPolicy Value { get; }

        /// <inheritdoc />
        public AnticipationPolicyKeyAttribute(BookingAnticipationPolicy value)
        {
            Value = value;
        }
    }
}