﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Services.AnticipationPolicies
{
    /// <summary>
    /// Anticipation policy factory
    /// </summary>
    public interface IAnticipationPolicyFactory
    {
        /// <summary>
        /// Test anticipation policy
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="booking"></param>
        /// <returns>error, message if has tested schedule, null otherwise</returns>
        Task<IDictionary<string, string>> TestAnticipationPolicy(string consortiumCode, BookingCreateViewModel booking);
    }
}