﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Plumbing;
using Api.ViewModels;

namespace Api.Services.AnticipationPolicies
{
    /// <inheritdoc />
    [AnticipationPolicyKey(BookingAnticipationPolicy.SevenDays)]
    public class SevenDaysBookingAnticipationPolicy : IBookingAnticipationPolicy
    {
        private readonly TimeSpan _sevenDays = TimeSpan.FromDays(7);

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestAnticipationPolicyAsync(string consortiumCode, BookingCreateViewModel booking)
        {
            var differenceUntilToday = booking.BookDate.Trim(TimeSpan.TicksPerDay) - DateTimeOffset.Now.Trim(TimeSpan.TicksPerDay);
            if (differenceUntilToday < _sevenDays)
            {
                return await Task.FromResult(new Dictionary<string, string>
                {
                    {"7DAYS_ANTICIPATION", "You can only book the facility with 7 days in advance"}
                });
            }

            return null;
        }
    }
}