﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Services.AnticipationPolicies
{
    /// <inheritdoc />
    public class AnticipationPolicyFactory : IAnticipationPolicyFactory
    {
        private readonly IFacilityRepository _facilityRepository;
        private readonly Func<BookingAnticipationPolicy, IBookingAnticipationPolicy> _factory;

        /// <inheritdoc />
        public AnticipationPolicyFactory(IFacilityRepository facilityRepository, 
            Func<BookingAnticipationPolicy, IBookingAnticipationPolicy> factory)
        {
            _facilityRepository = facilityRepository;
            _factory = factory;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestAnticipationPolicy(string consortiumCode, BookingCreateViewModel booking)
        {
            var strategy = await GetStrategyAsync(consortiumCode, booking.FacilityId);
            return await strategy.TestAnticipationPolicyAsync(consortiumCode, booking);
        }

        private async Task<IBookingAnticipationPolicy> GetStrategyAsync(string consortiumCode, int facilityId)
        {
            var facility = await _facilityRepository.GetAsync(consortiumCode, facilityId);
            var strategy = _factory(facility.AnticipationPolicy);
            return strategy;
        }       
    }
}