﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Plumbing;
using Api.ViewModels;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;

namespace Api.Services
{
    /// <inheritdoc />
    public class AuthManagementService : IAuthManagementService
    {
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public AuthManagementService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public async Task<User> CreateUser(UserCreateViewModel model, string consortiumCode, string functionalUnitCode)
        {
            var token = await GetAuthManagementToken();
            var domain = _configuration["Auth0:Domain"];
            var auth0 = new ManagementApiClient(token, domain);

            var request = new UserCreateRequest
            {
                AppMetadata = new
                {
                    consortiumCode,
                    functionalUnitCode
                },
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                EmailVerified = true,
                Connection = "email",
                FullName = $"{model.FirstName} {model.LastName}"
            };
            return await auth0.Users.CreateAsync(request);

        }

        /// <inheritdoc />
        public async Task<User> CreateAdministrator(AdministratorCreateViewModel model)
        {
            var token = await GetAuthManagementToken();
            var domain = _configuration["Auth0:Domain"];
            var auth0 = new ManagementApiClient(token, domain);

            var request = new UserCreateRequest
            {
                AppMetadata = new
                {
                    consortiumAdmin = "true",
                    consortiumCodes = model.ConsortiumCodes.ToArray()
                },
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Password = Guid.NewGuid().ToString("B"),
                EmailVerified = true,
                Connection = _configuration["Auth0:AdminConnection"],
                FullName = $"{model.FirstName} {model.LastName}"
            };
            var user = await auth0.Users.CreateAsync(request);

            return user;
        }

        private async Task<string> GetAuthManagementToken()
        {
            var parameters = new
            {
                grant_type = "client_credentials",
                client_id = _configuration["Auth0:ManagementClientId"],
                client_secret = _configuration["Auth0:ManagementClientSecret"],
                audience = _configuration["Auth0:ManagementApiIdentifier"]
            };

            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(parameters), ParameterType.RequestBody);

            var client = new RestClient($"https://{_configuration["Auth0:Domain"]}/oauth/token");
            var response = await client.ExecuteTaskAsync(request);
            dynamic content = JsonConvert.DeserializeObject(response.Content);

            return content.access_token;
        }
    }
}