﻿using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;

namespace Api.Services
{
    /// <summary>
    /// Auth0 management service
    /// </summary>
    public interface IAuthManagementService
    {
        /// <summary>
        /// Create auth0 user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="consortiumCode"></param>
        /// <param name="functionalUnitCode"></param>
        /// <returns></returns>
        Task<Auth0.ManagementApi.Models.User> CreateUser(UserCreateViewModel model, string consortiumCode, string functionalUnitCode);

        /// <summary>
        /// Create auth0 user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Auth0.ManagementApi.Models.User> CreateAdministrator(AdministratorCreateViewModel model);
    }
}