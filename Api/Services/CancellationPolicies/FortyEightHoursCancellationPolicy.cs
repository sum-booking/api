﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Plumbing;
using Api.Repositories;
using Api.ViewModels;

namespace Api.Services.CancellationPolicies
{
    /// <inheritdoc />
    [CancellationPolicyKey(BookingCancellationPolicy.FortyEightHours)]
    public class FortyEightHoursCancellationPolicy : ICancellationPolicy
    {
        private readonly IBookingRepository _repository;
        private readonly TimeSpan _fortyEightHours = TimeSpan.FromHours(48);

        /// <inheritdoc />
        public FortyEightHoursCancellationPolicy(IBookingRepository repository)
        {
            _repository = repository;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestCancellationPolicy(string consortiumCode, int bookingId)
        {
            var booking = await _repository.GetAsync(consortiumCode, bookingId);
            var differenceUntilToday = booking.BookDate.Trim(TimeSpan.TicksPerDay) - DateTimeOffset.Now.Trim(TimeSpan.TicksPerDay);
            if (differenceUntilToday < _fortyEightHours)
            {
                return await Task.FromResult(new Dictionary<string, string>
                {
                    {"48HOURS_CANCEL", "You can only cancel a booking 48 hours before"}
                });
            }

            return null;
        }
    }
}