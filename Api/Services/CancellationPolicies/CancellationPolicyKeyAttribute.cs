﻿using System;
using Api.Models.Enums;

namespace Api.Services.CancellationPolicies
{
    /// <inheritdoc />
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class CancellationPolicyKeyAttribute : Attribute
    {
        /// <summary>
        /// Value
        /// </summary>
        public BookingCancellationPolicy Value { get; }

        /// <inheritdoc />
        public CancellationPolicyKeyAttribute(BookingCancellationPolicy value)
        {
            Value = value;
        }
    }
}