﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.ViewModels;

namespace Api.Services.CancellationPolicies
{
    /// <summary>
    /// Cancellation policy
    /// </summary>
    public interface ICancellationPolicy
    {
        /// <summary>
        /// Test cancellation policy
        /// </summary>
        /// <param name="consortiumCode"></param>
        /// <param name="bookingId"></param>
        /// <returns>error, message if has tested schedule, null otherwise</returns>
        Task<IDictionary<string, string>> TestCancellationPolicy(string consortiumCode, int bookingId);
    }
}