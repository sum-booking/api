﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Plumbing;
using Api.Repositories;

namespace Api.Services.CancellationPolicies
{
    /// <inheritdoc />
    [CancellationPolicyKey(BookingCancellationPolicy.SeventyTwoHours)]
    public class SeventyTwoHoursCancellationPolicy : ICancellationPolicy
    {
        private readonly IBookingRepository _repository;
        private readonly TimeSpan _seventyTwoHours = TimeSpan.FromHours(72);

        /// <inheritdoc />
        public SeventyTwoHoursCancellationPolicy(IBookingRepository repository)
        {
            _repository = repository;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestCancellationPolicy(string consortiumCode, int bookingId)
        {
            var booking = await _repository.GetAsync(consortiumCode, bookingId);
            var differenceUntilToday = booking.BookDate.Trim(TimeSpan.TicksPerDay) - DateTimeOffset.Now.Trim(TimeSpan.TicksPerDay);
            if (differenceUntilToday < _seventyTwoHours)
            {
                return await Task.FromResult(new Dictionary<string, string>
                {
                    {"72HOURS_CANCEL", "You can only cancel a booking 72 hours before"}
                });
            }

            return null;
        }
    }
}