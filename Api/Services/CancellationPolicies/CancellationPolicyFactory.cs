﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Models.Enums;
using Api.Repositories;
using Api.Services.Schedules;
using Api.ViewModels;

namespace Api.Services.CancellationPolicies
{
    /// <inheritdoc />
    public class CancellationPolicyFactory : ICancellationPolicyFactory
    {
        private readonly IFacilityRepository _facilityRepository;
        private readonly Func<BookingCancellationPolicy, ICancellationPolicy> _factory;
        private readonly IBookingRepository _bookingRepository;

        /// <inheritdoc />
        public CancellationPolicyFactory(IFacilityRepository facilityRepository, 
            Func<BookingCancellationPolicy, ICancellationPolicy> factory, IBookingRepository bookingRepository)
        {
            _facilityRepository = facilityRepository;
            _factory = factory;
            _bookingRepository = bookingRepository;
        }

        /// <inheritdoc />
        public async Task<IDictionary<string, string>> TestCancellationPolicy(string consortiumCode, int bookingId)
        {
            var strategy = await GetStrategyAsync(consortiumCode, bookingId);
            return await strategy.TestCancellationPolicy(consortiumCode, bookingId);
        }

        private async Task<ICancellationPolicy> GetStrategyAsync(string consortiumCode, int bookingId)
        {
            var booking = await _bookingRepository.GetAsync(consortiumCode, bookingId);
            var facility = await _facilityRepository.GetAsync(consortiumCode, booking.FacilityId);
            var strategy = _factory(facility.CancellationPolicy);
            return strategy;
        }       
    }
}