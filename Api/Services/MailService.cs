﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Repositories;
using Api.ViewModels;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Api.Services
{
    /// <inheritdoc />
    public class MailService : IMailService
    {
        private readonly IConfiguration _configuration;
        private readonly IConsortiumRepository _consortiumRepository;

        /// <inheritdoc />
        public MailService(IConfiguration configuration, IConsortiumRepository consortiumRepository)
        {
            _configuration = configuration;
            _consortiumRepository = consortiumRepository;
        }

        /// <inheritdoc />
        public async Task SendWelcomeEmailAsync(UserViewModel model, string consortiumCode)
        {
            var apikey = _configuration["SendGrid:ApiKey"];
            var welcomeTemplateId = _configuration["SendGrid:WelcomeTemplateId"];

            var consortium = await _consortiumRepository.GetAsync(consortiumCode);

            var mailClient = new SendGridClient(apikey);
            var message = new SendGridMessage
            {
                From = new EmailAddress("bienvenido@zorzio.com", "Zorzio"),
                ReplyTo = new EmailAddress("soporte@zorzio.com", "Soporte Zorzio"),
                Subject = $"Bienvenido a Zorzio - Consorcio {consortium.Name}",
                TemplateId = welcomeTemplateId,
            };
            message.AddTo(new EmailAddress(model.Email, $"{model.FirstName} {model.LastName}"));
            await mailClient.SendEmailAsync(message);
        }
    }
}