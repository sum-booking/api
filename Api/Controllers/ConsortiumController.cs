﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Api.Repositories;
using Api.Validations;
using Api.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Controllers
{
    /// <summary>
    /// Consortium resource
    /// </summary>
    [ApiVersion("1.0")]
    [Route("v{api-version:apiVersion}/consortium")]
    public class ConsortiumController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IConsortiumRepository _consortiumRepository;

        /// <summary>
        /// Consortium constructor
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="consortiumRepository"></param>
        public ConsortiumController(IConfiguration configuration, IConsortiumRepository consortiumRepository)
        {
            _configuration = configuration;
            _consortiumRepository = consortiumRepository;
        }

        /// <summary>
        /// Get a list of consortiums
        /// </summary>
        /// <param name="offset">Offset to retrive (default 0)</param>
        /// <param name="limit">Limit of list (default 1000)</param>
        /// <returns>A list of consortiums</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No consortium founds</response>
        /// <response code="200">Consortiums retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(IEnumerable<Consortium>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllConsortiums([FromQuery]int? offset = 0, [FromQuery]int? limit = 1000)
        {
            var domain = $"https://{_configuration["Auth0:Domain"]}/";
            const string consortiumCodesType = "https://sumbooking.com/consortiumCodes";
            var consortiumCodes = User.Claims
                .Where(x => x.Issuer == domain && x.Type == consortiumCodesType)
                .Select(x => x.Value).ToList();

            var consortiums = await _consortiumRepository.GetAllAsync(consortiumCodes, offset, limit);

            if (!consortiums.Any())
            {
                return NotFound("No consortium founds");
            }

            return Ok(consortiums);
        }

        /// <summary>
        /// Get a consortium by code
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <returns>A consortium</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No consortium found</response>
        /// <response code="200">Consortium retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet("{consortiumCode}")]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(Consortium), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string consortiumCode)
        {

            var consortium = await _consortiumRepository.GetAsync(consortiumCode);
            if (consortium == null)
            {
                return NotFound("Consortium not found");
            }

            return Ok(consortium);
        }

        /// <summary>
        /// Create a new consortium
        /// </summary>
        /// <param name="model">The consortium to create</param>
        /// <returns>A created consortium</returns>
        /// <response code="400">Model is not well formed</response>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="409">Conflict with existing consortium</response>
        /// <response code="201">Consortium created</response>
        /// <response code="500">Internal error</response>
        [HttpPost]
        [Authorize("Root")]
        [ProducesResponseType(typeof(Consortium), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create([FromBody]ConsortiumCreateViewModel model)
        {
            var errors = ConsortiumValidations.ValidateCreate(model);

            if (errors.Any())
            {
                return BadRequest(errors);
            }

            var exists = await _consortiumRepository.ExistsAsync(model.Code);

            if (exists)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            var consortium = await _consortiumRepository.SaveAsync(model);

            return CreatedAtAction(nameof(GetById), new { consortiumCode = consortium.Code }, consortium);
        }

    }
}
