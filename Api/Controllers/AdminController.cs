﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Repositories;
using Api.Services;
using Api.Validations;
using Api.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Administrators resource
    /// </summary>
    [ApiVersion("1.0")]
    [Route("v{api-version:apiVersion}/admin")]
    public class AdminController : Controller
    {
        private readonly IAuthManagementService _authManagement;
        private readonly IAdminRepository _adminRepository;
        private readonly IAdministratorValidations _adminValidations;
        private readonly IConsortiumRepository _consortiumRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="authManagement"></param>
        /// <param name="adminRepository"></param>
        /// <param name="adminValidations"></param>
        /// <param name="consortiumRepository"></param>
        public AdminController(IAuthManagementService authManagement,
            IAdminRepository adminRepository, IAdministratorValidations adminValidations,
            IConsortiumRepository consortiumRepository)
        {
            _authManagement = authManagement;
            _adminRepository = adminRepository;
            _adminValidations = adminValidations;
            _consortiumRepository = consortiumRepository;
        }

        /// <summary>
        /// Get a list of administrators
        /// </summary>
        /// <param name="offset">Offset to retrive (default 0)</param>
        /// <param name="limit">Limit of list (default 1000)</param>
        /// <returns>A list of administrators</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No admins founds</response>
        /// <response code="200">Admins retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet]
        [Authorize("Root")]
        [ProducesResponseType(typeof(IEnumerable<AdministratorViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllAdmins([FromQuery] int? offset = 0, [FromQuery] int? limit = 1000)
        {
            var admins = await _adminRepository.GetAllAsync(offset, limit);

            if (!admins.Any())
            {
                return NotFound("No administrators found");
            }

            return Ok(admins);
        }

        /// <summary>
        /// Get an administrator by id
        /// </summary>
        /// <param name="administratorId">The Id of the administrator</param>
        /// <returns>A consortium</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No administrator found</response>
        /// <response code="200">Administrator retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet("{administratorId}")]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(AdministratorViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string administratorId)
        {
            var admin = await _adminRepository.GetAdministratorByIdAsync(administratorId);

            if (admin == null)
            {
                return NotFound("No administrator found");
            }

            return Ok(admin);
        }

        /// <summary>
        /// Create a new administrator
        ///  </summary>
        /// <param name="model">The administrator to create</param>
        /// <returns>A created administrator</returns>
        /// <response code="400">Model is not well formed</response>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="409">Conflict with existing administrator</response>
        /// <response code="201">Administrator created</response>
        /// <response code="500">Internal error</response>
        [HttpPost]
        [Authorize("Root")]
        [ProducesResponseType(typeof(AdministratorViewModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create([FromBody]AdministratorCreateViewModel model)
        {
            var errors = await _adminValidations.ValidateReferences(model);

            if (errors.Any())
            {
                return BadRequest(errors);
            }

            var exists = await _adminRepository.ConflictsAsync(model);

            if (exists)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            var authUser = await _authManagement.CreateAdministrator(model);

            await _adminRepository.SaveAsync(model, authUser.UserId, authUser.Picture);

            var tasks = new List<Task>();
            foreach (var consortiumCode in model.ConsortiumCodes)
            {
                tasks.Add(_consortiumRepository.UpdateAdministratorIdAsync(authUser.UserId, consortiumCode));
            }

            await Task.WhenAll(tasks);

            var user = await _adminRepository.GetAdministratorByIdAsync(authUser.UserId);

            return CreatedAtAction(nameof(GetById), new { administratorId = authUser.UserId }, user);
        }
    }
}