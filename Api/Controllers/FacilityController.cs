﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models;
using Api.Repositories;
using Api.Validations;
using Api.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Consortium facility resource
    /// </summary>
    [ApiVersion("1.0")]
    [Route("v{api-version:apiVersion}/consortium/{consortiumCode}/facility")]
    public class FacilityController : Controller
    {
        private readonly IFacilityRepository _facilityRepository;
        private readonly IFacilityValidations _facilityValidations;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="facilityRepository"></param>
        /// <param name="facilityValidations"></param>
        public FacilityController(IFacilityRepository facilityRepository,
            IFacilityValidations facilityValidations)
        {
            _facilityRepository = facilityRepository;
            _facilityValidations = facilityValidations;
        }

        /// <summary>
        /// Get a list of facilities by consortium
        /// </summary>
        /// <param name="consortiumCode">Consortium to get functional units for</param>
        /// <param name="offset">Offset to retrive (default 0)</param>
        /// <param name="limit">Limit of list (default 1000)</param>
        /// <returns>A list of facilities</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No facilities founds</response>
        /// <response code="200">Facilities retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(IEnumerable<Facility>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAll(string consortiumCode, [FromQuery]int? offset = 0, [FromQuery]int? limit = 1000)
        {
            var facilities = await _facilityRepository.GetAllAsync(consortiumCode, offset, limit);

            if (!facilities.Any())
            {
                return NotFound("No facilities found");
            }

            return Ok(facilities);
        }

        /// <summary>
        /// Get a facility by id
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="facilityId">The code of the facility</param>
        /// <returns>A consortium</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No facility found</response>
        /// <response code="200">Facility retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet("{facilityId}")]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(Facility), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string consortiumCode, int facilityId)
        {
            var facility = await _facilityRepository.GetAsync(consortiumCode, facilityId);

            if (facility == null)
            {
                return NotFound("Facility not found");
            }

            return Ok(facility);
        }

        /// <summary>
        /// Create a new facility
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="model">The facility to create</param>
        /// <returns>A created facility</returns>
        /// <response code="400">Model is not well formed</response>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="409">Conflict with existing facility</response>
        /// <response code="201">Facility created</response>
        /// <response code="500">Internal error</response>
        [HttpPost]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(Facility), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create(string consortiumCode, [FromBody]FacilityCreateViewModel model)
        {
            var errors = await _facilityValidations.ValidateReferences(model, consortiumCode);

            if (errors.Any())
            {
                return BadRequest(errors);
            }

            var exists = await _facilityRepository.ConflictsAsync(consortiumCode, model);

            if (exists)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            var facility = await _facilityRepository.SaveAsync(consortiumCode, model);

            return CreatedAtAction(nameof(GetById), new { consortiumCode, facilityId = facility.Id }, facility);
        }
    }
}