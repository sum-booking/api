﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models;
using Api.Repositories;
using Api.Validations;
using Api.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Consortium functional unit resource
    /// </summary>
    [ApiVersion("1.0")]
    [Route("v{api-version:apiVersion}/consortium/{consortiumCode}/functionalunit")]
    public class FunctionalUnitController : Controller
    {
        private readonly IFunctionalUnitValidations _functionalUnitValidations;
        private readonly IFunctionalUnitRepository _functionalUnitRepository;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="functionalUnitValidations"></param>
        /// <param name="functionalUnitRepository"></param>
        public FunctionalUnitController(IFunctionalUnitValidations functionalUnitValidations,
            IFunctionalUnitRepository functionalUnitRepository)
        {
            _functionalUnitValidations = functionalUnitValidations;
            _functionalUnitRepository = functionalUnitRepository;
        }

        /// <summary>
        /// Get a list of functional units by consortium
        /// </summary>
        /// <param name="consortiumCode">Consortium to get functional units for</param>
        /// <param name="offset">Offset to retrive (default 0)</param>
        /// <param name="limit">Limit of list (default 1000)</param>
        /// <returns>A list of consortiums</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No fucntional units founds</response>
        /// <response code="200">Functional units retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(IEnumerable<FunctionalUnit>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllFunctionalUnits(string consortiumCode, [FromQuery]int? offset = 0,
            [FromQuery]int? limit = 1000)
        {
            var functionalUnits = await _functionalUnitRepository.GetAllAsync(consortiumCode, offset, limit);

            if (!functionalUnits.Any())
            {
                return NotFound("No Functional units found");
            }

            return Ok(functionalUnits);
        }

        /// <summary>
        /// Get a functional unit by id
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="functionalUnitCode">The code of the functional unit</param>
        /// <returns>A consortium</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No functional unit found</response>
        /// <response code="200">Funtional unit retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet("{functionalUnitCode}")]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(FunctionalUnit), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string consortiumCode, string functionalUnitCode)
        {
            var functionalUnit = await _functionalUnitRepository.GetByIdAsync(consortiumCode, functionalUnitCode);

            if (functionalUnit == null)
            {
                return NotFound("Functional Unit not found");
            }

            return Ok(functionalUnit);
        }

        /// <summary>
        /// Create a new functional unit
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="model">The functiona unit to create</param>
        /// <returns>A created functional unit</returns>
        /// <response code="400">Model is not well formed</response>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="409">Conflict with existing functional unit</response>
        /// <response code="201">Functional unit created</response>
        /// <response code="500">Internal error</response>
        [HttpPost]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(FunctionalUnit), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create(string consortiumCode, [FromBody]FunctionalUnitCreateViewModel model)
        {
            var errors = await _functionalUnitValidations.ValidateReferences(model, consortiumCode);

            if (errors.Any())
            {
                return BadRequest(errors);
            }

            var exists = await _functionalUnitRepository.ConflictsAsync(model, consortiumCode);

            if (exists)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            var functionalUnit = await _functionalUnitRepository.SaveAsync(consortiumCode, model);

            return CreatedAtAction(nameof(GetById), new { consortiumCode, functionalUnitCode = functionalUnit.Code }, functionalUnit);
        }
    }
}