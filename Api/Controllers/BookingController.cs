﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Repositories;
using Api.Services.AnticipationPolicies;
using Api.Services.CancellationPolicies;
using Api.Services.Limits;
using Api.Services.Schedules;
using Api.Validations;
using Api.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Api.Controllers
{
    /// <summary>
    /// Booking resource
    /// </summary>
    [ApiVersion("1.0")]
    [Route("v{api-version:apiVersion}/consortium/{consortiumCode}/booking")]
    public class BookingController : Controller
    {
        private readonly IBookingRepository _repository;
        private readonly IBookingLimitStrategy _limitStrategy;
        private readonly ISchedulingStrategyFactory _schedulingStrategyFactory;
        private readonly ICancellationPolicyFactory _cancellationPolicyFactory;
        private readonly IAnticipationPolicyFactory _anticipationPolicyFactory;
        private readonly IBookingValidations _bookingValidations;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="limitStrategy"></param>
        /// <param name="schedulingStrategyFactory"></param>
        /// <param name="cancellationPolicyFactory"></param>
        /// <param name="anticipationPolicyFactory"></param>
        /// <param name="bookingValidations"></param>
        public BookingController(IBookingRepository repository, IBookingLimitStrategy limitStrategy,
            ISchedulingStrategyFactory schedulingStrategyFactory,
            ICancellationPolicyFactory cancellationPolicyFactory, IAnticipationPolicyFactory anticipationPolicyFactory,
            IBookingValidations bookingValidations)
        {
            _repository = repository;
            _limitStrategy = limitStrategy;
            _schedulingStrategyFactory = schedulingStrategyFactory;
            _cancellationPolicyFactory = cancellationPolicyFactory;
            _anticipationPolicyFactory = anticipationPolicyFactory;
            _bookingValidations = bookingValidations;
        }

        /// <summary>
        /// Get a list of bookings by consortium and date
        /// </summary>
        /// <param name="consortiumCode">Consortium to get bookings for</param>
        /// <param name="from">From date</param>
        /// <param name="to">To date</param>
        /// <param name="offset">Offset to retrive (default 0)</param>
        /// <param name="limit">Limit of list (default 1000)</param>
        /// <param name="userId">User id to get consortiums for</param>
        /// <returns>A list of bookings</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No bookings founds</response>
        /// <response code="200">Bookings retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(IEnumerable<BookingViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAll(string consortiumCode, [FromQuery] DateTimeOffset from,
            [FromQuery] DateTimeOffset to, [FromQuery] int? offset = 0, [FromQuery] int? limit = 1000, [FromQuery] string userId = null)
        {
            var result = await _repository.GetAllAsync(consortiumCode, from, to, offset, limit, userId: userId);

            if (!result.Any())
            {
                return NotFound("No bookings found");
            }

            return Ok(result);
        }

        /// <summary>
        /// Get a booking by id
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="bookingId">The id of the booking</param>
        /// <returns>A Booking</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No booking found</response>
        /// <response code="200">Booking retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet("{bookingId:int}")]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(BookingViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string consortiumCode, int bookingId)
        {
            var result = await _repository.GetAsync(consortiumCode, bookingId);

            if (result == null)
            {
                return NotFound("Booking not found");
            }

            return Ok(result);
        }

        /// <summary>
        /// Create a new booking
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="model">The booking to create</param>
        /// <returns>A created booking</returns>
        /// <response code="400">Model is not well formed</response>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="409">Conflict with existing booking</response>
        /// <response code="201">booking created</response>
        /// <response code="500">Internal error</response>
        [HttpPost]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(BookingViewModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create(string consortiumCode, [FromBody] BookingCreateViewModel model)
        {
            var errors = await _bookingValidations.ValidateReferences(model, consortiumCode);
            if (errors.Any())
            {
                return BadRequest(errors);
            }

            var strategyErrors = await _schedulingStrategyFactory.ValidateSchedulePropertiesAsync(consortiumCode, model);
            if (strategyErrors.Any())
            {
                return BadRequest(strategyErrors);
            }

            var limit = await _limitStrategy.TestLimitAsync(consortiumCode, model);
            if (limit != null)
            {
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(limit),
                    StatusCode = StatusCodes.Status409Conflict,
                    ContentType = @"application/json"
                };
            }

            var scheduleLimit = await _schedulingStrategyFactory.TestScheduleAsync(consortiumCode, model);
            if (scheduleLimit != null)
            {
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(scheduleLimit),
                    StatusCode = StatusCodes.Status409Conflict,
                    ContentType = @"application/json"
                };
            }

            var anticipationPolicy = await _anticipationPolicyFactory.TestAnticipationPolicy(consortiumCode, model);
            if (anticipationPolicy != null)
            {
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(anticipationPolicy),
                    StatusCode = StatusCodes.Status409Conflict,
                    ContentType = @"application/json"
                };
            }

            var id = await _repository.SaveAsync(consortiumCode, model);
            var booking = await _repository.GetAsync(consortiumCode, id);

            return CreatedAtAction(nameof(GetById), new { consortiumCode, bookingId = id }, booking);
        }

        /// <summary>
        /// Delete a booking by id
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="bookingId">The id of the booking</param>
        /// <param name="updatedBy">Updated by</param>
        /// <param name="updatedOn">Updated on</param>
        /// <returns></returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No booking found</response>
        /// <response code="200">Booking retrived</response>
        /// <response code="500">Internal error</response>
        [HttpDelete("{bookingId:int}")]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(BookingViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteById(string consortiumCode, int bookingId, [FromQuery]string updatedBy, [FromQuery]DateTimeOffset updatedOn)
        {
            var errors = new Dictionary<string, string>();

            if (String.IsNullOrWhiteSpace(updatedBy))
            {
                errors.Add("updatedBy", "Is required");
            }

            if (updatedOn <= DateTimeOffset.MinValue)
            {
                errors.Add("updatedOn", "Must be a valid date");
            }

            if (errors.Any())
            {
                return BadRequest(errors);
            }

            var result = await _repository.GetAsync(consortiumCode, bookingId);

            if (result == null)
            {
                return NotFound("Booking not found");
            }

            var testCancellation = await _cancellationPolicyFactory.TestCancellationPolicy(consortiumCode, bookingId);
            if (testCancellation != null)
            {
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(testCancellation),
                    StatusCode = StatusCodes.Status409Conflict,
                    ContentType = @"application/json"
                };
            }

            await _repository.DeleteAsync(consortiumCode, bookingId, updatedBy, updatedOn);

            return Ok();
        }
    }
}