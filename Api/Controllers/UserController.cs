﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Repositories;
using Api.Services;
using Api.Validations;
using Api.ViewModels;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Api.Controllers
{
    /// <summary>
    /// Functional unit user resource
    /// </summary>
    [ApiVersion("1.0")]
    [Route("v{api-version:apiVersion}/consortium/{consortiumCode}/functionalunit/{functionalUnitCode}/user")]
    [Route("v{api-version:apiVersion}/consortium/{consortiumCode}/user")]
    public class UserController : Controller
    {
        private readonly IAuthManagementService _authManagement;
        private readonly IMailService _mailService;
        private readonly IUserValidations _userValidations;
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="authManagement"></param>
        /// <param name="mailService"></param>
        /// <param name="userValidations"></param>
        /// <param name="userRepository"></param>
        public UserController(IAuthManagementService authManagement,
            IMailService mailService, IUserValidations userValidations, IUserRepository userRepository)
        {
            _authManagement = authManagement;
            _mailService = mailService;
            _userValidations = userValidations;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Get a list of users by functional unit and consortium code
        /// </summary>
        /// <param name="consortiumCode">Consortium code</param>
        /// <param name="functionalUnitCode">Functional unit code</param>
        /// <param name="offset">Offset to retrive (default 0)</param>
        /// <param name="limit">Limit of list (default 1000)</param>
        /// <returns>A list of users</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No users founds</response>
        /// <response code="200">Users retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(IEnumerable<UserViewModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllUsers(string consortiumCode, string functionalUnitCode = null,
            [FromQuery] int? offset = 0, [FromQuery] int? limit = 1000)
        {
            var users = await _userRepository.GetAllAsync(consortiumCode, functionalUnitCode, offset, limit);

            if (!users.Any())
            {
                return NotFound("No users found");
            }

            return Ok(users);
        }

        /// <summary>
        /// Get a user by id
        /// </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="functionalUnitCode">The code of the functional unit</param>
        /// <param name="userId">The Id of the user</param>
        /// <returns>A consortium</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">No userfound</response>
        /// <response code="200">Funtional unit retrived</response>
        /// <response code="500">Internal error</response>
        [HttpGet("{userId}")]
        [Authorize("FunctionalUnitUser")]
        [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string consortiumCode, string functionalUnitCode, string userId)
        {
            var user = await _userRepository.GetUserByIdAsync(consortiumCode, functionalUnitCode, userId);

            if (user == null)
            {
                return NotFound("No user found");
            }

            return Ok(user);
        }

        /// <summary>
        /// Create a new user
        ///  </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="functionalUnitCode">The code of the functional unit</param>
        /// <param name="model">The user to create</param>
        /// <returns>A created user</returns>
        /// <response code="400">Model is not well formed</response>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="409">Conflict with existing user</response>
        /// <response code="201">User created</response>
        /// <response code="500">Internal error</response>
        [HttpPost]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(UserViewModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create(string consortiumCode, string functionalUnitCode,
            [FromBody]UserCreateViewModel model)
        {
            var errors = await _userValidations.ValidateReferences(model, consortiumCode, functionalUnitCode);

            if (errors.Any())
            {
                return BadRequest(errors);
            }

            var exists = await _userRepository.CheckConflictsAsync(model);

            if (exists)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            var authUser = await _authManagement.CreateUser(model, consortiumCode, functionalUnitCode);

            await _userRepository.SaveAsync(consortiumCode, functionalUnitCode, authUser.UserId, authUser.Picture,
                model);

            var user = await _userRepository.GetUserByIdAsync(consortiumCode, functionalUnitCode, authUser.UserId);

            if (model.SendWelcomingMail)
            {
                await _mailService.SendWelcomeEmailAsync(user, consortiumCode);
            }

            return CreatedAtAction(nameof(GetById), new { consortiumCode, functionalUnitCode, authUser.UserId }, user);
        }

        /// <summary>
        /// Create a new user
        ///  </summary>
        /// <param name="consortiumCode">The code of the consortium</param>
        /// <param name="functionalUnitCode">The code of the functional unit</param>
        /// <param name="userId">The user id</param>
        /// <returns>A created user</returns>
        /// <response code="401">Unauthorized to access this resource</response>
        /// <response code="403">Forbidden to access the resource, scope is not right</response>
        /// <response code="404">Model is not well formed</response>
        /// <response code="200">Link send</response>
        /// <response code="500">Internal error</response>
        [HttpPost("resendlink/{userId}")]
        [Authorize("ConsortiumAdmin")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ResendWelcomingMail(string consortiumCode, string functionalUnitCode, string userId)
        {
            var user = await _userRepository.GetUserByIdAsync(consortiumCode, functionalUnitCode, userId);

            if (user == null)
            {
                return NotFound("User not found");
            }

            await _mailService.SendWelcomeEmailAsync(user, consortiumCode);

            return Ok("User link send");
        }
    }
}