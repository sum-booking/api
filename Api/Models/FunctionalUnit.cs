﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
    /// <summary>
    /// Functional unit
    /// </summary>
    public class FunctionalUnit : ModelBase
    {
        /// <summary>
        /// Consortium id
        /// </summary>
        [Required]
        public string ConsortiumCode { get; set; }

        /// <summary>
        /// Designation
        /// </summary>
        [Required]
        public string Designation { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [Required]
        public string Code { get; set; }
    }
}