﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
    /// <summary>
    /// User
    /// </summary>
    public class User : ModelBase
    {
        /// <summary>
        /// User id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Consortium id
        /// </summary>
        [Required]
        public string ConsortiumCode { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [Required]
        public string FunctionalUnitCode { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        [Required]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Picture url
        /// </summary>
        [Required]
        public string PictureUrl { get; set; }

        /// <summary>
        /// Phone notifications
        /// </summary>
        public bool PhoneNotifications { get; set; }

        /// <summary>
        /// Email notifications
        /// </summary>
        public bool EmailNotifications { get; set; }
    }
}