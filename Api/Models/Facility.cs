﻿using System.ComponentModel.DataAnnotations;
using Api.Models.Enums;

namespace Api.Models
{
    /// <summary>
    /// Facility
    /// </summary>
    public class Facility : ModelBase
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Consortium code
        /// </summary>
        [Required]
        public string ConsortiumCode { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Booking schedule strategy
        /// </summary>
        [Required]
        public BookingScheduleStrategy BookingSchedule { get; set; }

        /// <summary>
        /// Booking limit strategy
        /// </summary>
        [Required]
        public BookingLimitStrategy BookingLimit { get; set; }

        /// <summary>
        /// Booking cancellation policy
        /// </summary>
        [Required]
        public BookingCancellationPolicy CancellationPolicy { get; set; }

        /// <summary>
        /// Anticipation policy
        /// </summary>
        [Required]
        public BookingAnticipationPolicy AnticipationPolicy { get; set; }
    }
}