﻿using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
    /// <summary>
    /// Consortium model
    /// </summary>
    public class Consortium : ModelBase
    {
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Code
        /// </summary>
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// Administrator Id
        /// </summary>
        public string AdministratorId { get; set; }
    }
}