﻿using System;
using System.ComponentModel.DataAnnotations;
using Api.Models.Enums;

namespace Api.Models
{
    /// <summary>
    /// Booking
    /// </summary>
    public class Booking : ModelBase
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Consortium code
        /// </summary>
        [Required]
        public string ConsortiumCode { get; set; }

        /// <summary>
        /// Functional unit code
        /// </summary>
        [Required]
        public string FunctionalUnitCode { get; set; }

        /// <summary>
        /// Facility id
        /// </summary>
        [Required]
        public int FacilityId { get; set; }

        /// <summary>
        /// Book date
        /// </summary>
        [Required]
        public DateTimeOffset BookDate { get; set; }

        /// <summary>
        /// Strategy
        /// </summary>
        [Required]
        public BookingScheduleStrategy Strategy { get; set; }

        #region MidDayAnNightStrategy

        /// <summary>
        /// Is mid day booking
        /// </summary>
        public bool? IsMidDay { get; set; }

        /// <summary>
        /// Is night booking
        /// </summary>
        public bool? IsNight { get; set; }

        #endregion        
    }
}