﻿namespace Api.Models.Enums
{
    /// <summary>
    /// Booking limit strategy
    /// </summary>
    public enum BookingLimitStrategy
    {
        /// <summary>
        /// Twice a month
        /// </summary>
        TwiceAMonth = 0,
    }
}