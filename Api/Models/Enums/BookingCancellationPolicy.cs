﻿namespace Api.Models.Enums
{
    /// <summary>
    /// Booking cancellation policy
    /// </summary>
    public enum BookingCancellationPolicy
    {
        /// <summary>
        /// Sevent two hours
        /// </summary>
        SeventyTwoHours = 0,

        /// <summary>
        /// Forty eight hours
        /// </summary>
        FortyEightHours = 1,
    }
}