﻿namespace Api.Models.Enums
{
    /// <summary>
    /// Booking schedule strategy
    /// </summary>
    public enum BookingScheduleStrategy
    {
        /// <summary>
        /// Night only
        /// </summary>
        NightOnly = 0,

        /// <summary>
        /// Mid day and night
        /// </summary>
        MidDayAndNight = 1
    }
}