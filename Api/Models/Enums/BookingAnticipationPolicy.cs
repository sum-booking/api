﻿namespace Api.Models.Enums
{
    /// <summary>
    /// Booking anticipation policy
    /// </summary>
    public enum BookingAnticipationPolicy
    {
        /// <summary>
        /// Seven days
        /// </summary>
        SevenDays = 0,

        /// <summary>
        /// Two days
        /// </summary>
        TwoDays = 1,
    }
}