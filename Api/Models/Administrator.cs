﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
    /// <summary>
    /// Administrator
    /// </summary>
    public class Administrator : ModelBase
    {
        /// <summary>
        /// User id
        /// </summary>
        public string AdministratorId { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        [Required]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Picture url
        /// </summary>
        [Required]
        public string PictureUrl { get; set; }        
    }
}