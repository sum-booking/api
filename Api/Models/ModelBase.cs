﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Models
{
    /// <summary>
    /// Model base class
    /// </summary>
    public abstract class ModelBase
    {
        /// <summary>
        /// Created by
        /// </summary>
        [Required]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Created on with timezone
        /// </summary>
        [Required]
        public DateTimeOffset CreatedOn { get; set; }

        /// <summary>
        /// Updated by
        /// </summary>
        [Required]
        public string UpdatedBy { get; set; }

        /// <summary>
        /// Updated on with timezone
        /// </summary>
        [Required]
        public DateTimeOffset UpdatedOn { get; set; }

        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool Deleted { get; set; }
    }
}